
namespace AdventOfCode2023.UnitTests {
    public static class ReadPuzzleFromFile {
        public static string[] Read(string filename) {
            return File.ReadLines(Path.Join("../../../../../puzzles/", filename)).ToArray();
        }

    }
}