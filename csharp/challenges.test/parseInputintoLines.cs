
namespace AdventOfCode2023.UnitTests {

    public static class parseStringToArray {
        public static string[] parse(string input) {
            return input.ReplaceLineEndings().Split(Environment.NewLine, StringSplitOptions.None);
        }
    }
}
