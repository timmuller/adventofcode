using System.Globalization;
using AdventOfCode2023.Day2;

namespace AdventOfCode2023.UnitTests.Day2
{
    public class Task1Test
    {

        [Theory, MemberData(nameof(expectedGameObject))]
        public void parseGameToObject(string gameString, Game expectedGame) {
            var gameObject = Task1.convertGameStringToObject(gameString);
            Assert.Equal( expectedGame, gameObject);
        }

        public static IEnumerable<object[]> expectedGameObject => new List<object[]>
            {
                new object[] { "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", new Game("2") },
                new object[] { "Game 31: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", new Game("31") },
            };
        

//         [Fact]
//         public void TestGivenExample()
//         {
//             string[] gamesInput = parseStringToArray.parse(@"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
// Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
// Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
// Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
// Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green");

//             string restriction = @"12 red cubes, 13 green cubes, and 14 blue cubes";
//             int sumOfGameIds = Task1.solve(gamesInput);
//             Assert.Equal(8, sumOfGameIds);
//         }
    }
}