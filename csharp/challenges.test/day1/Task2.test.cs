using AdventOfCode2023.Day1;

namespace AdventOfCode2023.UnitTests.Day1 {

    public class Task2Test {

            [Theory]
            [InlineData("one", 11)]
            [InlineData("two", 22)]
            [InlineData("three", 33)]
            [InlineData("four", 44)]
            [InlineData("eighthree", 83)]
            public void testStringConversions(string input, int expectedSum) {
                Assert.Equal(expectedSum, Task2.solve([input]));
            }


        [Fact]
        public void testGivenExample() {
            string[] puzzleInput = parseStringToArray.parse(@"two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen");
            int puzzleOutput = Task2.solve(puzzleInput);
            Assert.Equal(281, puzzleOutput);
        }

        [Fact]
        public void TestChallenge()
        {
            string[] inputLines = ReadPuzzleFromFile.Read("2023/1.txt");
            var output = Task2.solve(inputLines);
            Assert.Equal(54845, output);

        }

    }
}