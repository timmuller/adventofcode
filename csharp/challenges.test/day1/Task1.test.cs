using AdventOfCode2023.Day1;

namespace AdventOfCode2023.UnitTests.Day1
{
    public class Task1Test
    {

        [Fact]
        public void TestGivenExample()
        {
            string[] exampleInput = parseStringToArray.parse(@"1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet");
            var output = Task1.solve(exampleInput);
            Assert.Equal(142, output);
        }

        [Fact]
        public void TestChallenge()
        {
            string[] inputLines = ReadPuzzleFromFile.Read("2023/1.txt");
            var output = Task1.solve(inputLines);
            Assert.Equal(55090, output);

        }
    }
}