﻿using System.Text.RegularExpressions;

namespace AdventOfCode2023.Day1 {
    public static class Task1 {
        public static int solve(string[] input) {
            var numbersOnly = input.Select(x => x.Where(Char.IsDigit)).Select(x => int.Parse(x.First().ToString() + x.Last().ToString())).ToArray();
            return numbersOnly.Sum();
        }
    }

    public static partial class Task2 {

        [GeneratedRegex(@"(\d|one|two|three|four|five|six|seven|eight|nine)")]
        private static partial Regex findNumbersLeft();

        [GeneratedRegex(@"(\d|one|two|three|four|five|six|seven|eight|nine)", RegexOptions.RightToLeft)]
        private static partial Regex findNumbersRight();

        static string stringToNumber(string str) => str switch
        {
            "one" => "1",
            "two" => "2",
            "three" => "3",
            "four" => "4",
            "five" => "5",
            "six" => "6",
            "seven" => "7",
            "eight" => "8",
            "nine" => "9",
            _ => str,
        };

        public static int solve(string[] input) {
            int[] numberStringsReplaced = input.Select(
                x => {
                    MatchCollection matchesLeft = findNumbersLeft().Matches(x);
                    MatchCollection matchesRight = findNumbersRight().Matches(x);
                    return int.Parse(stringToNumber(matchesLeft.First().Value) + stringToNumber(matchesRight.First().Value));
                }
            ).ToArray();

            return numberStringsReplaced.Sum();
        }
    }
}