using System.Text.RegularExpressions;

namespace AdventOfCode2023.Day2 {

    public record Game(string Id);


    public static partial class Task1 {


        [GeneratedRegex(@"^Game (\d+):( (.+?);)* (.+?)$")]
        private static partial Regex GameRegex();
        
        public static Game convertGameStringToObject(string gameInput)
        {
            var regexMatch = GameRegex().Match(gameInput);
            return new Game(regexMatch.Groups[1].Value);
        }

        public static int solve(string[] gamesInput) {
            return 0;
        }
    }
}