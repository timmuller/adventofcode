#!/bin/bash
set -e


pytest $@

flake8 .
mypy ./