import os


ABSOLUTE_PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
PUZZLE_PATH = os.path.join(ABSOLUTE_PROJECT_PATH, '..', '..', 'puzzles')
