
import re
from typing import Annotated
from fastapi import APIRouter, Body

router = APIRouter(prefix="/day/3")


type InputString = str
type SafeReportCount = int


@router.post('/solution/1', response_model=SafeReportCount, name="2024_3_1")
def day1(puzzle_lines: Annotated[InputString, Body()]):
    matches = re.findall(r'mul\((\d+),(\d+)\)', puzzle_lines)
    return sum([int(x) * int(y) for x, y in matches])


@router.post('/solution/2', response_model=SafeReportCount, name="2024_3_2")
def day2(puzzle_lines: Annotated[InputString, Body()]):
    matches = re.findall(r'(don\'t|do)|mul\((\d+),(\d+)\)', puzzle_lines)

    total_sum = 0
    is_valid = True
    for operator, x, y in matches:
        if operator == "don't":
            is_valid = False
        if operator == "do":
            is_valid = True

        if operator == '' and is_valid:
            total_sum += int(x) * int(y)

    return total_sum
