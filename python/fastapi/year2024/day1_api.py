
from typing import Annotated, List
from fastapi import APIRouter, Body

router = APIRouter(prefix="/day/1")


type InputStrings = str
type TotalDistance = int


@router.post('/solution/1', response_model=TotalDistance, name="2024_1_1")
def day1(puzzle_lines: Annotated[List[InputStrings], Body()]):
    left_numbers, right_numbers = sortedLeftAndRight(puzzle_lines)
    return sum(abs(right - left) for left, right in zip(left_numbers, right_numbers))


@router.post('/solution/2', response_model=TotalDistance, name="2024_1_2")
def day2(puzzle_lines: Annotated[List[InputStrings], Body()]):
    left_numbers, right_numbers = sortedLeftAndRight(puzzle_lines)
    return sum(
        map(lambda left: left * right_numbers.count(left), left_numbers)
    )


def sortedLeftAndRight(puzzle_lines):
    left_numbers, right_numbers = zip(
        *[map(int, line.split('   ', maxsplit=1)) for line in puzzle_lines]
    )
    return sorted(left_numbers), sorted(right_numbers)
