from typing import Annotated, Generator, List, Tuple
from fastapi import APIRouter, Body


router = APIRouter(prefix="/day/4")


type InputString = str
type Coordinate = Tuple[int, int]
type SafeReportCount = int


@router.post('/solution/1', response_model=SafeReportCount, name="2024_4_1")
def day1(puzzle_lines: Annotated[List[InputString], Body()]):
    MATCH_STRING = 'XMAS'
    return sum(
        coordinate_has_string_match(coordinate, puzzle_lines, MATCH_STRING)
        for coordinate in get_coordinates_of_char(puzzle_lines, MATCH_STRING[0])
    )


def get_coordinates_of_char(puzzle_lines: List[str], char_lookup: str) -> Generator[Coordinate, None, None]:
    for rowId, line in enumerate(puzzle_lines):
        for columnID, char in enumerate(line):
            if char == char_lookup:
                yield rowId, columnID


def find_strings_by_coordinate(coordinate: Coordinate, puzzle_lines: List[InputString], lookup_spaces: int):
    x, y = coordinate

    # horizontal
    if y + lookup_spaces <= len(puzzle_lines[0]):
        yield puzzle_lines[x][y:y + lookup_spaces]
    if y - lookup_spaces + 1 >= 0:
        yield ''.join(reversed(puzzle_lines[x][y - lookup_spaces + 1:y+1]))

    # vertical
    if x + lookup_spaces <= len(puzzle_lines):
        chars_down = [puzzle_lines[x+xdown][y] for xdown in range(0, lookup_spaces)]
        yield ''.join(chars_down)
    if x - lookup_spaces + 1 >= 0:
        chars_down = [puzzle_lines[x-xdown][y] for xdown in range(0, lookup_spaces)]
        yield ''.join(chars_down)

    # diagonal
    if x + lookup_spaces <= len(puzzle_lines):
        if y + lookup_spaces <= len(puzzle_lines[0]):
            yield ''.join([puzzle_lines[x + increment][y + increment] for increment in range(0, lookup_spaces)])
        if y - lookup_spaces + 1 >= 0:
            yield ''.join([puzzle_lines[x + increment][y - increment] for increment in range(0, lookup_spaces)])
    if x - lookup_spaces + 1 >= 0:
        if y + lookup_spaces <= len(puzzle_lines[0]):
            yield ''.join([puzzle_lines[x - increment][y + increment] for increment in range(0, lookup_spaces)])
        if y - lookup_spaces + 1 >= 0:
            yield ''.join([puzzle_lines[x - increment][y - increment] for increment in range(0, lookup_spaces)])

    return StopIteration()


def coordinate_has_string_match(coordinate, puzzle_lines, string_match):
    return len(list(
        filter(
            lambda matchString: matchString == string_match,
            find_strings_by_coordinate(coordinate, puzzle_lines, len(string_match))
        )
    ))


@router.post('/solution/2', response_model=SafeReportCount, name="2024_4_2")
def day2(puzzle_lines: Annotated[List[InputString], Body()]):
    return sum(is_x_mas(coordinate, puzzle_lines) for coordinate in get_coordinates_of_char(puzzle_lines, 'A'))


def is_x_mas(coordinate, puzzle_lines):
    try:
        rowId, columnId = coordinate
        if rowId - 1 < 0 or columnId - 1 < 0:
            return False

        match1 = [puzzle_lines[rowId-1][columnId-1], puzzle_lines[rowId+1][columnId+1]]
        match2 = [puzzle_lines[rowId-1][columnId+1], puzzle_lines[rowId+1][columnId-1]]
        if sorted(match1) == sorted(match2) and sorted(match1) == ['M', 'S']:
            return True
        return False
    except IndexError:
        return False
