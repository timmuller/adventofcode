
from typing import Annotated, List
from fastapi import APIRouter, Body

router = APIRouter(prefix="/day/2")


type InputStrings = str
type SafeReportCount = int


def conclude_level_differences(reportInput: str):
    levels = list(map(int, reportInput.split()))
    return [prev - next for prev, next in zip(levels[0: -1], levels[1:])]


@router.post('/solution/1', response_model=SafeReportCount, name="2024_2_1")
def day1(puzzle_lines: Annotated[List[InputStrings], Body()]):
    return len(list(filter(is_save_puzzle1, puzzle_lines)))


def is_save_puzzle1(reportInput: str):
    level_differences = conclude_level_differences(reportInput)
    is_increasing = all(level > 0 and level <= 3 for level in level_differences)
    if is_increasing:
        return True
    return all(level < 0 and level >= -3 for level in level_differences)


@router.post('/solution/2', response_model=SafeReportCount, name="2024_2_2")
def day2(puzzle_lines: Annotated[List[InputStrings], Body()]):
    return len(list(filter(is_save_puzzle2, puzzle_lines)))


def is_save_puzzle2(report_input: str):
    if is_save_puzzle1(report_input):
        return True

    levels_input = report_input.split()
    for index in range(len(levels_input)):
        try:
            adaptedString = " ".join(levels_input[:index] + levels_input[index+1:])
            if is_save_puzzle1(adaptedString):
                return True
        except TypeError:
            return False
    return False
