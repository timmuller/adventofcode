from typing import Annotated, List, Tuple
from fastapi import APIRouter, Body


router = APIRouter(prefix="/day/6")


GUARD_SIGN = '#'


DIRECTION_UP = '^'
DIRECTION_LEFT = '>'
DIRECTION_RIGHT = '<'
DIRECTION_DOWN = 'v'


type InputString = str
type PuzzleResult = int
type Coordinate = Tuple[int, int]


class NoGuardFoundError(Exception):
    pass


def investigate_map(puzzle_input: str):
    start_point: Coordinate | None = None
    guard_coordinates: List[Coordinate] = []

    for row_index, line in enumerate(puzzle_input.splitlines()):
        for column_index, char in enumerate(line):
            if char == DIRECTION_UP:
                start_point = (row_index, column_index)
            if char == GUARD_SIGN:
                guard_coordinates.append((row_index, column_index))
    return start_point, guard_coordinates


def get_visited_locations(start_point, guard_locations, direction, visited_locations, max_dimension):
    coordinatesLookup = None
    if direction == DIRECTION_UP:
        coordinatesLookup = ((rowIndex, start_point[1]) for rowIndex in reversed(range(0, start_point[0])))
    if direction == DIRECTION_DOWN:
        coordinatesLookup = ((rowIndex, start_point[1]) for rowIndex in range(start_point[0] + 1, max_dimension))
    if direction == DIRECTION_RIGHT:
        coordinatesLookup = ((start_point[0], columnIndex) for columnIndex in range(start_point[1] + 1, max_dimension))
    if direction == DIRECTION_LEFT:
        coordinatesLookup = ((start_point[0], columnIndex) for columnIndex in reversed(range(0, start_point[1])))

    for coordinate_lookup in coordinatesLookup:
        if coordinate_lookup not in guard_locations:
            yield coordinate_lookup
        else:
            return StopIteration()
    raise NoGuardFoundError()


def endless_direction_generator():
    orders = [DIRECTION_UP, DIRECTION_RIGHT, DIRECTION_DOWN, DIRECTION_LEFT]
    for order in orders:
        yield order
    yield from endless_direction_generator()


@router.post('/solution/1', response_model=PuzzleResult, name="2024_6_1")
def day1(puzzle_input: Annotated[InputString, Body()]):
    start_point, guard_locations = investigate_map(puzzle_input)
    maximum_dimension = len(puzzle_input.splitlines()[0])

    visited_locations = [start_point]
    for move_direction in endless_direction_generator():
        try:
            for visited_location in get_visited_locations(
                visited_locations[-1], guard_locations, move_direction, visited_locations, maximum_dimension
            ):
                visited_locations.append(visited_location)
        except NoGuardFoundError:
            break

    return len(list(set(visited_locations)))


@router.post('/solution/2', response_model=PuzzleResult, name="2024_6_2")
def day2(puzzle_input: Annotated[InputString, Body()]):
    return 0
