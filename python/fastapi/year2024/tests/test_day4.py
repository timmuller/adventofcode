
from main import app
import pytest


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2024_4_1')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2024_4_2')


@pytest.fixture()
def minimal_input_vertical_and_horizontal():
    return """...S...
...A...
...M...
SAMXMAS
...M...
...A...
...S...
""".splitlines()


@pytest.fixture()
def minimal_input_diagonal():
    return """S.....S
.A...A.
..M.M..
...X...
..M.M..
.A...A.
S.....S
""".splitlines()


@pytest.fixture()
def example_input():
    return """MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX""".splitlines()


@pytest.fixture()
def minimal_input_x_mas():
    return """M.S
.A.
M.S
""".splitlines()


@pytest.fixture()
def example_input_xmas():
    return """.M.S......
..A..MSMS.
.M.S.MAA..
..A.ASMSM.
.M.S.M....
..........
S.S.S.S.S.
.A.A.A.A..
M.M.M.M.M.
..........
""".splitlines()


class TestSolution1:
    def test_url_exists(self, solution1_route):
        assert solution1_route == '/aoc/api/2024/day/4/solution/1'

    def test_minimal_example_horizontal_and_vertical(
        self, solution1_route, client, minimal_input_vertical_and_horizontal
    ):
        response = client.post(solution1_route, json=minimal_input_vertical_and_horizontal)

        assert response.json() == 4

    def test_minimal_diagonal(self, solution1_route, client, minimal_input_diagonal):
        response = client.post(solution1_route, json=minimal_input_diagonal)

        assert response.json() == 4

    def test_example(self, solution1_route, client, example_input):
        response = client.post(solution1_route, json=example_input)

        assert response.json() == 18

    def test_puzzle_input(self, solution1_route, client, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines('2024', '4')

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 2427


class TestSolution2:
    def test_url_exists(self, solution2_route):
        assert solution2_route == '/aoc/api/2024/day/4/solution/2'

    def test_minimal_example_horizontal_and_vertical(
        self, solution2_route, client, minimal_input_x_mas
    ):
        response = client.post(solution2_route, json=minimal_input_x_mas)

        assert response.json() == 1

    def test_example(self, solution2_route, client, example_input_xmas):
        response = client.post(solution2_route, json=example_input_xmas)

        assert response.json() == 9

    def test_puzzle_input(self, solution2_route, client, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines('2024', '4')

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 1900
