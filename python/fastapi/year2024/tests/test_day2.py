
from main import app
import pytest


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2024_2_1')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2024_2_2')


@pytest.fixture()
def example_input():
    return """7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9""".splitlines()


class TestSolution1:
    def test_url_exists(self, solution1_route):
        assert solution1_route == '/aoc/api/2024/day/2/solution/1'

    def test_example(self, solution1_route, client, example_input):
        response = client.post(solution1_route, json=example_input)

        assert response.json() == 2

    def test_puzzle_input(self, solution1_route, client, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines('2024', '2')

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 220


class TestSolution2:
    def test_url_exists(self, solution2_route):
        assert solution2_route == '/aoc/api/2024/day/2/solution/2'

    def test_example(self, solution2_route, client, example_input):
        response = client.post(solution2_route, json=example_input)

        assert response.json() == 4

    def test_additional_string(self, solution2_route, client):
        inputString = [
            '3 1 2 4 5',
            '1 2 4 5 4',
            '4 5 4 2 1',
            '5 4 2 1 3',
            '53 51 52 54 55',
            '51 52 54 55 54',
            '55 54 52 51 53',
            '54 55 54 52 51',
            '44 43 40 39 37 36 33 31',
            '1 2 3 4 5',
            '1 2 4 3 5',
            '1 3 2 4 5',
        ]
        response = client.post(solution2_route, json=inputString)

        assert response.json() == 12

    def test_puzzle_input(self, solution2_route, client, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines('2024', '2')

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 296
