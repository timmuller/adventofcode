
from main import app
import pytest


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2024_3_1')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2024_3_2')


@pytest.fixture()
def example_input():
    return "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"


@pytest.fixture()
def example_input2():
    return "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"


class TestSolution1:
    def test_url_exists(self, solution1_route):
        assert solution1_route == '/aoc/api/2024/day/3/solution/1'

    def test_example(self, solution1_route, client, example_input):
        response = client.post(solution1_route, json=example_input)

        assert response.json() == 161

    def test_puzzle_input(self, solution1_route, client, fullPuzzleInput):
        puzzleInput = fullPuzzleInput('2024', '3')

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 167650499


class TestSolution2:
    def test_url_exists(self, solution2_route):
        assert solution2_route == '/aoc/api/2024/day/3/solution/2'

    def test_example(self, solution2_route, client, example_input2):
        response = client.post(solution2_route, json=example_input2)

        assert response.json() == 48

    def test_puzzle_input(self, solution2_route, client, fullPuzzleInput):
        puzzleInput = fullPuzzleInput('2024', '3')

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 95846796
