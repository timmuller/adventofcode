
from main import app
import pytest


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2024_1_1')


@pytest.fixture()
def exampleInput():
    return """3   4
4   3
2   5
1   3
3   9
3   3
""".splitlines()


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2024_1_2')


class TestSolution1:
    def test_url_exists(self, solution1_route):
        assert solution1_route == '/aoc/api/2024/day/1/solution/1'

    def test_example(self, solution1_route, client, exampleInput):
        response = client.post(solution1_route, json=exampleInput)

        assert response.json() == 11

    def test_puzzle_input(self, solution1_route, client, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines('2024', '1')

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 2196996


class TestSolution2:
    def test_url_exists(self, solution2_route):
        assert solution2_route == '/aoc/api/2024/day/1/solution/2'

    def test_example(self, solution2_route, client, exampleInput):
        response = client.post(solution2_route, json=exampleInput)

        assert response.json() == 31

    def test_puzzle_input(self, solution2_route, client, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines('2024', '1')

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 23655822
