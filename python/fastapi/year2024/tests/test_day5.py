
from main import app
import pytest


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2024_5_1')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2024_5_2')


@pytest.fixture()
def example_input():
    return """47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47"""


class TestSolution1:
    def test_url_exists(self, solution1_route):
        assert solution1_route == '/aoc/api/2024/day/5/solution/1'

    def test_example(self, solution1_route, client, example_input):
        response = client.post(solution1_route, json=example_input)

        assert response.json() == 143

    def test_puzzle_input(self, solution1_route, client, fullPuzzleInput):
        puzzleInput = fullPuzzleInput('2024', '5')

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 5651


class TestSolution2:
    def test_url_exists(self, solution2_route):
        assert solution2_route == '/aoc/api/2024/day/5/solution/2'

    def test_example(self, solution2_route, client, example_input):
        response = client.post(solution2_route, json=example_input)

        assert response.json() == 123

    def test_puzzle_input(self, solution2_route, client, fullPuzzleInput):
        puzzleInput = fullPuzzleInput('2024', '5')

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 4743
