
from main import app
import pytest


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2024_6_1')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2024_6_2')


@pytest.fixture()
def example_input():
    return """....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#..."""


class TestSolution1:
    def test_url_exists(self, solution1_route):
        assert solution1_route == '/aoc/api/2024/day/6/solution/1'

    def test_example(self, solution1_route, client, example_input):
        response = client.post(solution1_route, json=example_input)

        assert response.json() == 41

    def test_puzzle_input(self, solution1_route, client, fullPuzzleInput):
        puzzleInput = fullPuzzleInput('2024', '6')

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 4890


# class TestSolution2:
#     def test_url_exists(self, solution2_route):
#         assert solution2_route == '/aoc/api/2024/day/5/solution/2'

#     def test_example(self, solution2_route, client, example_input):
#         response = client.post(solution2_route, json=example_input)

#         assert response.json() == 123

#     def test_puzzle_input(self, solution2_route, client, fullPuzzleInput):
#         puzzleInput = fullPuzzleInput('2024', '5')

#         response = client.post(solution2_route, json=puzzleInput)

#         assert response.json() == 4743
