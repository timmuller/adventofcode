from collections import defaultdict
import math
from typing import Annotated, List
from fastapi import APIRouter, Body


router = APIRouter(prefix="/day/5")


type InputString = str
type PuzzleResult = int


def parse_puzzle_input(input: InputString):
    rules, printOrders = [], []
    found_all_rules = False
    for line in input.splitlines():
        if line == '':
            found_all_rules = True
            continue
        if not found_all_rules:
            rules.append(line)
        else:
            printOrders.append(list(map(int, line.split(','))))
    return rules, printOrders


def parse_rules(rules: List[str]):
    group_by_page = defaultdict(list)
    for x, y in (map(int, rule.split('|', maxsplit=1)) for rule in rules):
        group_by_page[x].append(y)
    return group_by_page


@router.post('/solution/1', response_model=PuzzleResult, name="2024_5_1")
def day1(puzzle_input: Annotated[InputString, Body()]):
    rules, printorders = parse_puzzle_input(puzzle_input)
    groupedRules = parse_rules(rules)
    return sum(get_middle_page(printorder) for printorder in printorders if is_valid_order(printorder, groupedRules))


def is_valid_order(printorder: List[int], groupedRules):
    previous_page_numbers = []
    for print_page_number in printorder:
        leftPageLookup = groupedRules[print_page_number]
        if any(leftLookup in previous_page_numbers for leftLookup in leftPageLookup):
            return False
        previous_page_numbers.append(print_page_number)
    return True


def get_middle_page(printorder: str):
    return printorder[math.ceil(len(printorder) / 2) - 1]


@router.post('/solution/2', response_model=PuzzleResult, name="2024_5_2")
def day2(puzzle_input: Annotated[InputString, Body()]):
    rules, printorders = parse_puzzle_input(puzzle_input)
    groupedRules = parse_rules(rules)
    return sum(get_middle_page(
        restructure(printorder, groupedRules))
        for printorder in printorders if not is_valid_order(printorder, groupedRules)
    )


def restructure(printorder, groupedRules):
    corrected_order = []
    for print_page_number in printorder:
        preventedCharacters = groupedRules[print_page_number]
        mistakeChars = list(
            sorted([
                corrected_order.index(preventChar)
                for preventChar in preventedCharacters if preventChar in corrected_order
            ])
        )
        if mistakeChars:
            insertIndex = mistakeChars[0]
            corrected_order.insert(insertIndex, print_page_number)
        else:
            corrected_order.append(print_page_number)
    return corrected_order
