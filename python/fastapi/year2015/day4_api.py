from typing import Annotated
from fastapi import APIRouter, Body
import hashlib


type InputHash = str

router = APIRouter(prefix="/day/4")


@router.post('/solution/1', name="2015_4_1")
def solution1(input_hash: Annotated[InputHash, Body()]):
    return first_leading_zeros(input_hash, expected_leading_numbers=5)


@router.post('/solution/2', name="2015_4_2")
def solution2(input_hash: Annotated[InputHash, Body()]):
    return first_leading_zeros(input_hash, expected_leading_numbers=6)


def first_leading_zeros(input_hash: InputHash, expected_leading_numbers: int) -> str:
    start = "1" + "0" * expected_leading_numbers
    end = int(start + "0") - 1
    for i in range(int(start), end):
        string_to_process = f'{input_hash}{i}'
        calculated_hash = hashlib.md5(string_to_process.encode()).hexdigest()
        if calculated_hash.startswith('0' * expected_leading_numbers):
            return str(i)
    raise RuntimeError("Mistakes have been made, all combinations didn't reult in anything")
