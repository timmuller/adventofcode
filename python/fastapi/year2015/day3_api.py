from typing import Annotated, List
from fastapi import APIRouter, Body


router = APIRouter(prefix="/day/3")

type Instruction = str
type Coordinate = str

MOVING_EAST = ">"
MOVING_WEST = "<"
MOVING_NORTH = "^"
MOVING_SOUTH = "v"


@router.post("/solution/1", name="2015_3_1")
def solution1(instructions: Annotated[Instruction, Body()]):
    visitedCoordinates: List[Coordinate] = []
    current_coordinate: Coordinate = "0,0"
    visitedCoordinates.append(current_coordinate)

    for instruction in instructions:
        current_coordinate = get_next_coordinate(current_coordinate, instruction)
        visitedCoordinates.append(current_coordinate)

    return len(set(visitedCoordinates))


def get_next_coordinate(current_coordinate: Coordinate, instruction: Instruction) -> Coordinate:
    x, y = map(int, current_coordinate.split(',', maxsplit=1))

    if instruction == MOVING_EAST:
        y = y + 1
    if instruction == MOVING_WEST:
        y = y - 1
    if instruction == MOVING_NORTH:
        x = x - 1
    if instruction == MOVING_SOUTH:
        x = x + 1

    return f'{x},{y}'


@router.post('/solution/2', name="2015_3_2")
def solution2(instructions: Annotated[Instruction, Body()]):
    visitedCoordinates: List[Coordinate] = []
    current_coordinate_santa: Coordinate = "0,0"
    current_coordinate_robo: Coordinate = "0,0"
    visitedCoordinates.append(current_coordinate_santa)

    for index, instruction in enumerate(instructions):
        if index % 2:
            current_coordinate_santa = get_next_coordinate(current_coordinate_santa, instruction)
            visitedCoordinates.append(current_coordinate_santa)
        else:
            current_coordinate_robo = get_next_coordinate(current_coordinate_robo, instruction)
            visitedCoordinates.append(current_coordinate_robo)

    return len(set(visitedCoordinates))
