from fastapi import APIRouter
from year2015 import day1_api, day2_api, day3_api, day4_api, day5_api

router = APIRouter(prefix="/aoc/api/2015")
router.include_router(day1_api.router)
router.include_router(day2_api.router)
router.include_router(day3_api.router)
router.include_router(day4_api.router)
router.include_router(day5_api.router)
