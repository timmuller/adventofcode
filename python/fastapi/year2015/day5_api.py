from typing import Annotated, Counter, List
from fastapi import APIRouter, Body


type InputString = str
type IsNiceString = bool


VOWELS = 'aeiou'
BLACKLISTED_COMBINATIONS = ['ab', 'cd', 'pq', 'xy']

router = APIRouter(prefix="/day/5")


@router.post('/solution/1', name="2015_5_1")
def solution1(puzzleInputs: Annotated[List[InputString], Body()]):
    nice_strings = filter(isNice, puzzleInputs)
    return len(list(nice_strings))


@router.post('/solution/2', name="2015_5_2")
def solution2(puzzleInputs: Annotated[List[InputString], Body()]):
    nice_strings = filter(isNice2, puzzleInputs)
    return len(list(nice_strings))


@router.post('/solution/1/isnice', name="2015_5_isnice", response_model=IsNiceString)
def isNice(inputString: Annotated[InputString, Body()]) -> IsNiceString:
    character_count = Counter(inputString)

    vowels_count = [character_count[vowel] for vowel in VOWELS]
    if sum(vowels_count) < 3:
        return False

    count_followups = count_duplicate_followup_chars(inputString)
    if count_followups == 0:
        return False

    if any(blacklist_combo in inputString for blacklist_combo in BLACKLISTED_COMBINATIONS):
        return False

    return True


def count_duplicate_followup_chars(inputString, last_char=None):
    if inputString == '':
        return 0

    current_char, rest_chars = inputString[0], inputString[1:]
    if last_char is None:
        return count_duplicate_followup_chars(rest_chars, current_char)
    if last_char == current_char:
        return 1 + count_duplicate_followup_chars(rest_chars, current_char)
    return 0 + count_duplicate_followup_chars(rest_chars, current_char)


def isNice2(inputString) -> IsNiceString:
    at_least_twice = atLeastTwoPairRepeats(inputString)
    repeats = repeatingChar(inputString)
    return repeats and at_least_twice


def atLeastTwoPairRepeats(inputString):
    return any(inputString[i:i+2] in inputString[i+2:] for i in range(len(inputString) - 2))


def repeatingChar(inputString):
    return any(
        inputString[index - 1] == inputString[index + 1]
        for index in range(len(inputString))
        if index != 0 and index + 1 < len(inputString)
    )
