
from typing import Annotated, Optional
from fastapi import APIRouter, Body, HTTPException, status

router = APIRouter(prefix="/day/1")


type PuzzleInputOneLine = str
type PuzzleOutputNumber = int


LEVEL_UP_CHAR: str = "("
LEVEL_DOWN_CHAR: str = ")"


@router.post('/solution/1', response_model=PuzzleOutputNumber, name="2015_1_1")
def day1(puzzle_input: Annotated[PuzzleInputOneLine, Body()]):
    validate_puzzleInput(puzzle_input=puzzle_input)

    return puzzle_input.count(LEVEL_UP_CHAR) - puzzle_input.count(LEVEL_DOWN_CHAR)


@router.post('/solution/2', name="2015_1_2", response_model=PuzzleOutputNumber)
def day2(puzzle_input: Annotated[PuzzleInputOneLine, Body()]):
    validate_puzzleInput(puzzle_input=puzzle_input)

    index_below_zero = find_below_zero_index(puzzle_input)
    if index_below_zero is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Never goes below zero"
        )
    return index_below_zero + 1


def find_below_zero_index(puzzle_input: PuzzleInputOneLine) -> Optional[int]:
    currentLevel = 0
    for index, movement in enumerate(puzzle_input):
        if movement == LEVEL_UP_CHAR:
            currentLevel = currentLevel + 1
        if movement == LEVEL_DOWN_CHAR:
            currentLevel = currentLevel - 1

        if currentLevel < 0:
            return index
    return None


def validate_puzzleInput(puzzle_input: PuzzleInputOneLine):
    if not puzzle_input:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Puzzle input is empty"
        )
