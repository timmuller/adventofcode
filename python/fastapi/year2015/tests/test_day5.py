
from main import app
import pytest


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2015_5_1')


@pytest.fixture()
def isnice_route():
    return app.url_path_for('2015_5_isnice')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2015_5_2')


@pytest.mark.parametrize('inputString,isNice', [
    ['ugknbfddgicrmopn', True],
    ['aaa', True],
    ['x', False],
    ['jchzalrnumimnmhp', False],
    ['haegwjzuvuyypxyu', False],
    ['dvszwmarrgswjxmb', False]
])
def test_is_nice_string(isnice_route, client, inputString, isNice):
    response = client.post(isnice_route, json=inputString)

    assert response.json() == isNice


class TestSolutionOne:
    def test_url_exists(self, solution1_route):
        assert solution1_route == "/aoc/api/2015/day/5/solution/1"

    def test_example_list(self, solution1_route, client):
        example_input = [
            'ugknbfddgicrmopn',
            'aaa',
            'x',
            'jchzalrnumimnmhp',
            'haegwjzuvuyypxyu',
            'dvszwmarrgswjxmb',
            'ugknbfgicrmopndd',
            'ddugknbfgicrmopn',
        ]

        response = client.post(solution1_route, json=example_input)

        assert response.json() == 4

    def test_puzzle_input(self, solution1_route, client, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines('2015', '5')

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 255


class TestSolutionTwo:
    def test_url_exists(self, solution2_route):
        assert solution2_route == "/aoc/api/2015/day/5/solution/2"

    def test_nice_example_strings(self, solution2_route, client):
        nice_strings = [
            'xyazaxy',
            'aabcecaa',
            'aabceccaa',
            'qjhvhtzxzqqjkmpb',
            'xxyxx'
        ]
        nonnice_strings = [
            'a',
            'aaa',
            'adsf',
            'uurcxstgmygtbstg',
            'ieodomkazucvgmuy'
        ]

        response = client.post(solution2_route, json=nice_strings + nonnice_strings)

        assert response.json() == 5

    def test_puzzle_input(self, solution2_route, client, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines('2015', '5')

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 55
