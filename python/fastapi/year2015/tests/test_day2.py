import pytest
from main import app


@pytest.fixture()
def detail_route():
    return app.url_path_for('2015_2_1_detailroute')


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2015_2_1')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2015_2_2')


class TestSolutionOne:
    def test_url_exists(self, detail_route):
        assert detail_route == "/aoc/api/2015/day/2/solution/1/single"

    @pytest.mark.parametrize('dimension,expectedPaper', [
        ["2x3x4", 58],
        ["1x1x10", 43]
    ])
    def test_wrapping_paper_response(self, client, detail_route, dimension, expectedPaper):
        response = client.post(detail_route, json=dimension)

        assert response.status_code == 200
        assert response.json() == expectedPaper

    def test_solution_example(self, client, solution1_route):
        dimensions = [
            '2x3x4',
            '1x1x10',
        ]
        response = client.post(solution1_route, json=dimensions)

        assert response.status_code == 200
        assert response.json() == 101

    def test_puzzle_input(self, client, solution1_route, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines(2015, 2)

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 1586300


class TestSolutionTwo:
    def test_route_exists(self, solution2_route):
        assert solution2_route == '/aoc/api/2015/day/2/solution/2'

    @pytest.mark.parametrize('dimension,expectedRibbon', [
        ["2x3x4", 34],
        ["1x1x10", 14]
    ])
    def test_get_ribbon_example(self, solution2_route, client, dimension, expectedRibbon):
        response = client.post(solution2_route, json=[dimension])

        assert response.json() == expectedRibbon

    def test_example_puzzle_input(self, client, solution2_route, fullPuzzleInputLines):
        puzzleInput = [
            '2x3x4',
            '1x1x10',
            '10x2x1'
        ]

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 74

    def test_puzzle_input(self, client, solution2_route, fullPuzzleInputLines):
        puzzleInput = fullPuzzleInputLines(2015, 2)

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 3737498
