import pytest
from main import app


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2015_3_1')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2015_3_2')


class TestSolutionOne:
    def test_url_exists(self, solution1_route):
        assert solution1_route == "/aoc/api/2015/day/3/solution/1"

    @pytest.mark.parametrize("instructions,expectedHouseDeliveries", [
        ['>', 2],
        ['>>', 3],
        ['><', 2],
        ['^>v<', 4],
        ['^v^v^v^v^v', 2]
    ])
    def test_small_set_deliveries(self, solution1_route, client, instructions, expectedHouseDeliveries):
        response = client.post(solution1_route, json=instructions)
        assert response.json() == expectedHouseDeliveries

    def test_puzzle_input_deliveries(self, solution1_route, client, fullPuzzleInput):
        puzzleInput = fullPuzzleInput('2015', '3')

        response = client.post(solution1_route, json=puzzleInput)

        assert response.json() == 2565


class TestSolutionTwo:
    def test_url_exists(self, solution2_route):
        assert solution2_route == "/aoc/api/2015/day/3/solution/2"

    @pytest.mark.parametrize("instructions,expectedHouseDeliveries", [
        ['><', 3],
        ['^v', 3],
        ['^>v<', 3],
        ['^v^v^v^v^v', 11]
    ])
    def test_small_set_deliveries(self, solution2_route, client, instructions, expectedHouseDeliveries):
        response = client.post(solution2_route, json=instructions)
        assert response.json() == expectedHouseDeliveries

    def test_puzzle_input_deliveries(self, solution2_route, client, fullPuzzleInput):
        puzzleInput = fullPuzzleInput('2015', '3')

        response = client.post(solution2_route, json=puzzleInput)

        assert response.json() == 2639
