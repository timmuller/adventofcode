import pytest
from main import app


class TestFirstSolution:
    def test_route_exist(self):
        assert app.url_path_for('2015_1_1') == '/aoc/api/2015/day/1/solution/1'

    def test_api_returns_validationError_when_puzzle_input_is_empty(self, client):
        emptyPuzzleInput = ''

        response = client.post(app.url_path_for('2015_1_1'), json=emptyPuzzleInput)

        assert response.status_code == 400
        assert response.json() == {'detail': 'Puzzle input is empty'}

    @pytest.mark.parametrize('puzzleInput,expectedLevel', [
        ['(())', 0],
        ['()()', 0],
        ['(', 1],
        [')', -1],
        ['))(((((', 3]
    ])
    def test_api_response_for_puzzle_input(self, client, puzzleInput, expectedLevel):
        response = client.post(app.url_path_for('2015_1_1'), json=puzzleInput)

        assert response.status_code == 200
        assert response.json() == expectedLevel

    def test_full_puzzle_input(self, client, fullPuzzleInput):
        response = client.post(app.url_path_for('2015_1_1'), json=fullPuzzleInput(2015, 1))

        assert response.status_code == 200
        assert response.json() == 74


class TestSecondSolution:
    def test_route_exist(self):
        assert app.url_path_for('2015_1_2') == '/aoc/api/2015/day/1/solution/2'

    def test_api_returns_validation_error_on_no_puzzle_input(self, client):
        response = client.post(app.url_path_for('2015_1_2'), json='')

        assert response.status_code == 400
        assert response.json() == {'detail': 'Puzzle input is empty'}

    def test_api_returns_validation_error_when_never_goes_below_zero(self, client):
        puzzleInput = "("

        response = client.post(app.url_path_for('2015_1_2'), json=puzzleInput)

        assert response.status_code == 400
        assert response.json() == {'detail': 'Never goes below zero'}

    @pytest.mark.parametrize('puzzleInput,expectedIndex', [
        [')', 1],
        ['())', 3],
        ['()())', 5],
    ])
    def test_api_with_example_input(self, client, puzzleInput, expectedIndex):
        response = client.post(app.url_path_for('2015_1_2'), json=puzzleInput)

        assert response.json() == expectedIndex

    def test_full_puzzle_input(self, client, fullPuzzleInput):
        response = client.post(app.url_path_for('2015_1_2'), json=fullPuzzleInput(2015, 1))

        assert response.json() == 1795
