from main import app
import pytest


@pytest.fixture()
def solution1_route():
    return app.url_path_for('2015_4_1')


@pytest.fixture()
def solution2_route():
    return app.url_path_for('2015_4_2')


class TestSolutionOne:
    def test_url_exists(self, solution1_route):
        assert solution1_route == "/aoc/api/2015/day/4/solution/1"

    def test_example_hash(self, solution1_route, client):
        test_hash = "abcdef"
        expected_addition = "609043"

        response = client.post(solution1_route, json=test_hash)

        assert response.status_code == 200
        assert response.json() == expected_addition

    def test_puzzle_input(self, solution1_route, client):
        test_hash = "bgvyzdsv"

        response = client.post(solution1_route, json=test_hash)

        assert response.status_code == 200
        assert response.json() == '254575'


class TestSolutionTwo:
    def test_url_exists(self, solution2_route):
        assert solution2_route == "/aoc/api/2015/day/4/solution/2"

    def test_puzzle_input(self, solution2_route, client):
        test_hash = "bgvyzdsv"

        response = client.post(solution2_route, json=test_hash)

        assert response.status_code == 200
        assert response.json() == '1038736'
