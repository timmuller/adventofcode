from typing import Annotated, Iterable, List
from fastapi import APIRouter, Body


type Dimension = str
type WrappingPaper = int
type Ribbon = int


router = APIRouter(prefix="/day/2")


@router.post('/solution/1/single', name="2015_2_1_detailroute", response_model=WrappingPaper)
def wrapping_paper(dimension: Annotated[Dimension, Body()]) -> WrappingPaper:
    length, width, height = get_lenghts(dimension)

    sides = [(length * width), (width * height), (height * length)]
    smallest_side = min(sides)
    return (sum(sides) * 2) + smallest_side


@router.post('/solution/1', name="2015_2_1", response_model=WrappingPaper)
def solution_one(dimensions: Annotated[List[Dimension], Body()]) -> WrappingPaper:
    return sum(map(wrapping_paper, dimensions))


@router.post('/solution/2', name='2015_2_2', response_model=Ribbon)
def solution_two(dimensions: Annotated[List[Dimension], Body()]) -> Ribbon:
    return sum(map(get_ribbon_length, dimensions))


def get_ribbon_length(dimension: Dimension) -> Ribbon:
    length, width, height = sorted(get_lenghts(dimension))
    return (length + length) + (width + width) + (length * width * height)


def get_lenghts(dimension: Dimension) -> Iterable[int]:
    return map(int, dimension.split('x', maxsplit=2))
