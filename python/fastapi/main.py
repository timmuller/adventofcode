from typing import Union

from fastapi import FastAPI
from year2015 import router2015
from year2024 import router2024

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}


app.include_router(router2015.router)
app.include_router(router2024.router)
