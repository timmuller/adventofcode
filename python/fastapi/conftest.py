
import os
from typing import List
from fastapi.testclient import TestClient
import pytest
from main import app
from config import PUZZLE_PATH


@pytest.fixture(scope="session")
def client():
    return TestClient(app)


@pytest.fixture
def fullPuzzleInput():
    def retrievePuzzleInput(puzzle_year: int, puzzle_day: int) -> str:
        puzzle_input_path = os.path.join(PUZZLE_PATH, f'{puzzle_year}_{puzzle_day}', 'input.txt')
        with open(puzzle_input_path, 'r') as fh:
            return fh.read()
    return retrievePuzzleInput


@pytest.fixture
def fullPuzzleInputLines(fullPuzzleInput):
    def retrievePuzzleInput(puzzle_year: int, puzzle_day: int) -> List[str]:
        return fullPuzzleInput(puzzle_year, puzzle_day).splitlines()
    return retrievePuzzleInput
