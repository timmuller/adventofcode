from unittest import TestCase
from day1.main import solution1, solution2
import os


class TestSolution1(TestCase):
    def test_example_input(self):
        exampleInput = """1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"""

        result = solution1(exampleInput)
        self.assertEqual(result, 142)
    
    def test_actual_input(self):
        absolutePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input.txt')
        with open(absolutePath, 'r') as fh:
            result = solution1(fh.read())
            self.assertEqual(result, 55090)


class TestSolution2(TestCase):
    def test_example_input(self):
        exampleInput = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"""
        result = solution2(exampleInput)
        self.assertEqual(result, 281)
    
    def test_example_input2(self):
        exampleInputs = [
            ["1three3", 13],
            ["six1mpffbnbnnlxthree", 63],
            ["1three", 13],
            ["three1", 31],
            ["oneightwoneight", 18],
        ]
        for inputValue, expectedOutput in exampleInputs:
            self.assertEqual(solution2(inputValue), expectedOutput)
    
    def test_actual_input(self):
        absolutePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input.txt')
        with open(absolutePath, 'r') as fh:
            result = solution2(fh.read())
            self.assertEqual(result, 54845)