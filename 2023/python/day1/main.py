def lookupIndex(chars, lookup):
    try:
        return chars.index(lookup)
    except Exception:
        return None

def lookupIndexes(chars, lookup, addIndex=0):
    foundIndex = lookupIndex(chars, lookup)
    if foundIndex is None:
        return []
    nextNeedle = foundIndex + len(lookup)

    nextIndexes = lookupIndexes(chars[nextNeedle:], lookup, addIndex=nextNeedle + addIndex)
    foundIndex = foundIndex + addIndex
    if nextIndexes:
        return [foundIndex] + nextIndexes
    return [foundIndex]


def findFirstNumber(chars):
    try:
        numberLookup = (char for char in chars if char.isnumeric())
        return next(numberLookup)
    except StopIteration:
        return None


def concludeNumber(stringIndex, chars, end=False):
    leftChars = chars[:stringIndex[0]] if end is False else reversed(chars[stringIndex[0]:])

    firstNumberLookup = findFirstNumber(leftChars)
    if(firstNumberLookup):
        return firstNumberLookup
    return stringIndex[1]


def findFirstNumberStrings(chars):
    # Learned after completing way simpler: Replace string to number then use solution 1 to continue


    stringNumbers = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    stringIndexes = []

    lowerChars = chars.lower()
    for numberIndex, stringNumber in enumerate(stringNumbers):
        foundStringIndexes = lookupIndexes(lowerChars, stringNumber)
        if foundStringIndexes:
            for foundStringIndex in foundStringIndexes:
                stringIndexes.append([foundStringIndex, numberIndex + 1])

    if not stringIndexes:
       return findValues(lowerChars)

    stringIndexes.sort(key=lambda x: x[0])
    firstNumber = concludeNumber(stringIndexes[0], lowerChars)
    lastNumber = concludeNumber(stringIndexes[-1], lowerChars, end=True)
    
    return int(str(firstNumber) + str(lastNumber));



def findValues(chars):
    firstNumber = findFirstNumber(chars)
    lastNumber = findFirstNumber(reversed(chars))
    return int(firstNumber + lastNumber)

def solution1(inputdata):
    inputLines = inputdata.splitlines()
    firstLastNumbers = [findValues(line) for line in inputLines]
    return sum(firstLastNumbers)

def solution2(inputdata):
    inputLines = inputdata.splitlines()
    firstLastNumbers = [findFirstNumberStrings(line) for line in inputLines]

    return sum(firstLastNumbers)
