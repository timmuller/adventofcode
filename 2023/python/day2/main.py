from collections import defaultdict
from functools import reduce


def parseLinetoGame(line):
    game, setString = line.split(':', maxsplit=1)
    _, gameId = game.split(' ')
    setStrings = setString.split(';')
    gameSets = defaultdict(dict)
    
    for index, setString in enumerate(setStrings):
        cubes = setString.strip().split(',')
        for cube in cubes:
            numberCubes, color = cube.strip().split(' ')
            gameSets[index][color] = int(numberCubes)

    return gameId, gameSets


def isPossibleGameTurn(turn, limit):
    return all([turn[color] <= limit[color] for color in turn.keys()])

def isPossibleGame(game, limit):
    return all(isPossibleGameTurn(gameTurn, limit) for gameTurn in game.values())


def solution1(input):
    limitGame = {
        'red': 12,
        'green': 13,
        'blue': 14
    }

    count = 0
    lines = input.splitlines()
    for line in lines:
        gameId, gameset = parseLinetoGame(line)
        if isPossibleGame(gameset, limitGame):
            count = count + int(gameId)
    return count


def minimalCubes(gameset):
    cubeCount = defaultdict(list)

    for gameSet in gameset.values():
        for color, value in gameSet.items():
            cubeCount[color].append(value)

    maxCubes = list(max(cubeCount[color]) for color in cubeCount.keys())
    return reduce(lambda a, b: a * b, maxCubes)


def solution2(input):
    lines = input.splitlines()
    count = 0
    for line in lines:
        _, gamesets = parseLinetoGame(line)
        count = count + minimalCubes(gamesets)
    return count