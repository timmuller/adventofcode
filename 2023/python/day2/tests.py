import os
from unittest import TestCase

from day2.main import solution1, solution2


class TestSolution1(TestCase):
    def test_example_input(self):
        exampleInput = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""
        result = solution1(exampleInput)
        self.assertEqual(result, 8)
    
    def test_actual_input(self):
        absolutePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input.txt')
        with open(absolutePath, 'r') as fh:
            result = solution1(fh.read())
            self.assertEqual(result, 2256)


class TestSolution2(TestCase):
    def test_example_input(self):
        exampleInput = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""
        result = solution2(exampleInput)
        self.assertEqual(result, 2286)

    def test_actual_input(self):
        absolutePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input.txt')
        with open(absolutePath, 'r') as fh:
            result = solution2(fh.read())
            self.assertEqual(result, 74229)