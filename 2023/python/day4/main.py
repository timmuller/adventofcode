

from collections import Counter, defaultdict
from typing import DefaultDict


def solvePuzzle1(games: list[str]):
    points = [concludeScore(game) for game in games]
    return sum(points)

def solvePuzzle2(games: list[str]):
    scoreCards: DefaultDict[int, int] = defaultdict(int)

    for gameString in games:
        gameId, draws, myNumbers = _parseGameString(gameString)
        matchingDrawsCount = _getNumberOfMatches(draws, myNumbers)
        wonAdditionalScoreCards = list(range(gameId + 1 , gameId + matchingDrawsCount + 1))
        numberOfScoreCardsCopies = scoreCards[gameId]

        scoreCards[gameId] += 1
        for _ in range(0, numberOfScoreCardsCopies+1):
            for additionalGameId in wonAdditionalScoreCards:
                scoreCards[additionalGameId] += 1

    return sum(scoreCards.values());

def concludeScore(gameString: str):
    gameId, draws, myNumbers = _parseGameString(gameString)

    matchingDrawsCount = _getNumberOfMatches(draws, myNumbers)
    if not matchingDrawsCount:
        return 0
    return 2 ** (matchingDrawsCount - 1)

def _getNumberOfMatches(draws, myNumbers):
    return len([draw for draw in draws if draw in myNumbers])

def _parseGameString(gameString: str):
    gameIdString, gameSet = gameString.split(':', maxsplit=2)
    draws, myNumbers = map(_strToNumbers, gameSet.split('|', maxsplit=2))
    _, gameId = gameIdString.split(' ', maxsplit=1)

    return int(gameId), draws, myNumbers


def _strToNumbers(numberString: str) -> list[str]:
    return [number.strip() for number in numberString.split(' ') if number != '']