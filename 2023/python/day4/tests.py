
import os
from unittest import TestCase
from day4.main import solvePuzzle1, solvePuzzle2


class TestAdventOfCodeDay4Puzzle1(TestCase):
    def test_with_sample(self):
        sample = """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
""".splitlines()
        
        self.assertEqual(solvePuzzle1(sample), 13)

    def test_puzzle(self) -> None:
        absolutePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input.txt')

        with open(absolutePath, 'r') as fh:
            result = solvePuzzle1(fh.readlines())
            self.assertEqual(result, 28750)

class TestAdventOfCodeDay4Puzzle2(TestCase):
    def test_with_sample(self):
        sample = """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11""".splitlines()
        
        self.assertEqual(solvePuzzle2(sample), 30)
 
    def test_puzzle(self) -> None:
        absolutePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input.txt')

        with open(absolutePath, 'r') as fh:
            result = solvePuzzle2(fh.readlines())
            self.assertEqual(result, 10212704)
