from collections import namedtuple

PartNumber = namedtuple('PartNumber', ['number', 'startIndex', 'endIndex'])


def hasSymbol(input: str):
    return len([x for x in input if x != '.' and not x.isdigit()]) != 0


def hasSymbolNeighour(puzzleInput: list[str], partNumber: PartNumber, currentLineIndex: int):
    lookupLines = [x for x in range(currentLineIndex - 1, currentLineIndex + 2)]
    lookupColumns = [x for x in range(partNumber.startIndex - 1, partNumber.endIndex + 2) if x >= 0]
    for line in lookupLines:
        try:
            if hasSymbol(puzzleInput[line][lookupColumns[0]:lookupColumns[-1]]):
                return True
        except IndexError:
            pass
    return False


def identifyNextNumber(puzzleLine: str):
    startNumberIndex = None
    currentNumber = ''

    for index, char in enumerate(puzzleLine):
        if char.isdigit():
            if startNumberIndex is None:
                startNumberIndex = index
            currentNumber += char
        elif startNumberIndex is not None:
            yield PartNumber(currentNumber, startNumberIndex, startNumberIndex + len(currentNumber))
            currentNumber = ''
            startNumberIndex = None
    if startNumberIndex is not None:
        yield PartNumber(currentNumber, startNumberIndex, startNumberIndex + len(currentNumber))
    return StopIteration()


def solvePartNumber(puzzleInput: str) -> int:
    puzzleLines = puzzleInput.splitlines()

    partNumbers = []

    for lineIndex, line in enumerate(puzzleLines):
        for number in identifyNextNumber(line):
            if (hasSymbolNeighour(puzzleLines, number, lineIndex)):
                partNumbers.append(number)

    return sum(int(partNumber.number) for partNumber in partNumbers)


def findGearNumbers(puzzleInput: list[str]):
    for lineIndex, columnIndex in findGearCoordinates(puzzleInput):
        neightbourLines = range(lineIndex - 1, lineIndex + 2)
        neightbourColums = range(columnIndex - 1, columnIndex + 2)

        print(neightbourLines, neightbourColums)

        yield 0


def findGearCoordinates(puzzleInput: list[str]):
    for lineIndex, puzzleStr in enumerate(puzzleInput):
        indexes = (index for index, char in enumerate(puzzleStr) if char == '*')
        for index in indexes:
            yield [lineIndex, index]
    return StopIteration()


def solvePartNumberWithGears(puzzleInput: str) -> int:
    puzzleLines = puzzleInput.splitlines()
    list(findGearNumbers(puzzleLines))
    return 0
    # partNumbers = []
    # for lineIndex, line in enumerate(puzzleLines):
    #     for number in identifyNextNumber(line):
    #         if (hasSymbolNeighour(puzzleLines, number, lineIndex)):
    #             partNumbers.append(number)
    # return sum(int(partNumber.number) for partNumber in partNumbers)