import os
from unittest import TestCase
from day3.main import solvePartNumber, solvePartNumberWithGears


class TestAdventOfCodeDay3Puzzle1(TestCase):
    def test_example_input(self) -> None:
        example_input: str = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598.."""

        part_number: int = solvePartNumber(example_input)

        self.assertEqual(part_number, 4361)

    def test_debugging(self) -> None:
        example_input: str = """..........
...*......
..35..6333
......#..."""

        part_number: int = solvePartNumber(example_input)

        self.assertEqual(part_number, 6368)

    def test_puzzle1(self) -> None:
        absolutePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input.txt')

        with open(absolutePath, 'r') as fh:
            result = solvePartNumber(fh.read())
            self.assertEqual(result, 557705)


class TestAdventOfCodeDay3Puzzle2(TestCase):
    pass
#     def test_example_input(self) -> None:
#         example_input: str = """467..114..
# ...*......
# ..35..633.
# ......#...
# 617*......
# .....+.58.
# ..592.....
# ......755.
# ...$.*....
# .664.598.."""

#         part_number: int = solvePartNumberWithGears(example_input)

#         self.assertEqual(part_number, 467835)
    
#     def test_debug(self) -> None:
#         example_input: str = """467..114..
# ...*......"""

#         part_number: int = solvePartNumberWithGears(example_input)

#         self.assertEqual(part_number, 467835)