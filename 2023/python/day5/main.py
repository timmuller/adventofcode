from functools import reduce


def groupMapLocations(seedLines: list[str]):
    maps = []
    keyValues = []
    for line in seedLines :
        if ':' in line:
            if len(keyValues):
                maps.append(keyValues)
                keyValues = []
        elif len(line) != 0:
            keyValues.append(list(map(int, line.split(' '))))
        else:
            continue
    return maps

def _parseSeedLine(seedLine: str):
    _, seedNumber = seedLine.split(': ')
    return list(map(int, seedNumber.split(' ')))
        

def findNextPath(sourceLocations, mapLines):
    sourcesToConclude = sorted(sourceLocations)
    newDestinations = []


    for sourceToConclude in sourcesToConclude:
        concluded = False
        for destinationStart, sourceStart, rangeNumber in mapLines:
            if sourceToConclude >= sourceStart and sourceToConclude <= sourceStart + rangeNumber - 1:
                indexDestination = sourceToConclude - sourceStart
                destinationValue = indexDestination + destinationStart
                newDestinations.append(destinationValue)
                concluded = True
        if not concluded:
            newDestinations.append(sourceToConclude)

    return newDestinations



    # sourcesHandled = []
    # sortedMapLines = sorted(mapLines, key=lambda x: x[1]) 
    # for destinationStart, sourceStart, rangeNumber in sortedMapLines:
    #     for sourceToConclude in sourcesToConclude:
    #         if sourceToConclude in sourcesHandled:
    #             continue
    #         elif sourceToConclude < sourceStart:
    #             newDestinations.append(sourceToConclude)
    #             sourcesHandled.append(sourceToConclude)
    #         elif sourceToConclude <= sourceStart + rangeNumber:
    #             indexDestination = sourceToConclude - sourceStart
    #             destinationValue = indexDestination + destinationStart
    #             newDestinations.append(destinationValue)
    #             sourcesHandled.append(sourceToConclude)
    
    # for sourceToConclude in sourcesToConclude:
    #     if sourceToConclude not in sourcesHandled:
    #         newDestinations.append(sourceToConclude)

    # if(len(sourcesToConclude) != len(newDestinations)):
    #     print('error, mismatching length', sourcesToConclude, newDestinations)
    # return newDestinations

def solvePuzzle1(seedInput: str):
    seedLines = seedInput.splitlines()
    seeds = _parseSeedLine(seedLines[0])
    locationMaps = groupMapLocations(seedLines[2:])

    locations = reduce(findNextPath, locationMaps, seeds)
    print(locations)
    return min(locations)