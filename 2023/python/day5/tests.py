
import os
from unittest import TestCase
from day5.main import solvePuzzle1


class TestAdventOfCodeDay5Puzzle1(TestCase):
    def test_with_sample(self):
        sample = """seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"""
        
        self.assertEqual(solvePuzzle1(sample), 35)
        
    def test_puzzle(self) -> None:
        absolutePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input.txt')

        with open(absolutePath, 'r') as fh:
            result = solvePuzzle1(fh.read())
            self.assertEqual(result, 303894138)