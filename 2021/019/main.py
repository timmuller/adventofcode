import functools
import itertools
from collections import deque

TEST="""[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"""


CHAR_COMBINATIONS = {
    '(': ')',
    '[': ']',
    '<': '>',
    '{': '}'
}
CLOSED_MAPPING = dict([(y, x) for x, y in CHAR_COMBINATIONS.items()])

def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def solve_puzzle(lines):
    incorrupt_ending_char = []
    for line in lines:
        not_closed_char = get_incorrectly_closed(line)
        if not_closed_char:
            incorrupt_ending_char.append(not_closed_char)

    print('puzzle1', calculate_incorrupt(incorrupt_ending_char))


def calculate_incorrupt(chars):
    values = {
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137
    }
    return sum([values.get(char) for char in chars])


def get_incorrectly_closed(line):
    queue = deque()
    for index, char in enumerate(line):
        if char in CHAR_COMBINATIONS:
            queue.append(char)
        else:
            starting_char = queue.pop()
            if CLOSED_MAPPING.get(char) != starting_char:
                return char
    return None

if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    # input_data = TEST
    parsed_data = input_data.splitlines()
    solve_puzzle(parsed_data)