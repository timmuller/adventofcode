from collections import Counter
from itertools import combinations, product

TEST = """0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"""


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def parse_raw_coordinate(raw_coordinate):
    x, y = raw_coordinate.split(',')
    return int(x), int(y)


def parse_to_coordinates(raw_input):
    line_coordinates = []
    for line in raw_input.splitlines():
        start, end = line.split(" -> ")
        start_x, start_y = parse_raw_coordinate(start)
        end_x, end_y = parse_raw_coordinate(end)

        range_x = get_number_range(start_x, end_x)
        range_y = get_number_range(start_y, end_y)

        if len(range_x) == 1 or len(range_y) == 1:
            line_coordinates.append(list(product(range_x, range_y)))
        else:
            line_coordinates.append(list(zip(range_x, range_y)))

    return line_coordinates


def get_number_range(start, end):
    if start < end:
        return [current for current in range(start, end + 1)]
    else:
        return list(reversed([current for current in range(end, start + 1)]))
    return [start]




if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    # input_data = TEST

    coordinates = parse_to_coordinates(input_data)


    flat_coordinates = sum(coordinates, [])
    counter = Counter(flat_coordinates)

    crossed_lines_count = 0
    for coordinate, counts_marked in counter.items():
        if counts_marked > 1:
            print(coordinate)
            crossed_lines_count += 1

    # 20898
    print(crossed_lines_count)