from collections import Counter, defaultdict
import time


TEST = """be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"""


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()

{
    ''
    'eafb': 4,
    'dab': 7,
}

def convert_to_digit(digit_sequence):
    digit_sequence.split().inter


def determine_signal(line):
    _, four_digit_output = line.split('|')
    return four_digit_output.strip().split(' ')


def get_all(input_data):
    all = []
    for line in input_data.splitlines():
        all += determine_signal(line)
    return all


if __name__ == "__main__":
    # input_data = TEST
    input_data = get_input_data('input.txt')

    length_to_digit = {
        2: [1],
        3: [7],
        4: [4],
        7: [8],
    }

    print(len([True for signal in get_all(input_data) if len(signal) in length_to_digit]))

