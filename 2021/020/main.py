import functools
import itertools
from collections import deque

TEST="""[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"""


CHAR_COMBINATIONS = {
    '(': ')',
    '[': ']',
    '<': '>',
    '{': '}'
}
CLOSED_MAPPING = dict([(y, x) for x, y in CHAR_COMBINATIONS.items()])

def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def solve_puzzle(lines):
    syntax_error_ending_chars = []
    ending_line_calculations = []
    for line in lines:
        is_unclosed_line, not_closed_char = get_incorrectly_closed(line)
        if not is_unclosed_line:
            # syntax error
            syntax_error_ending_chars.append(not_closed_char)
        else:

            ending_line_calculations.append(calculate_ending_line_value(not_closed_char))

    # 268845
    print('puzzle1', calculate_incorrupt(syntax_error_ending_chars))
    # 4038824534
    print('puzzle2', get_puzzle2(ending_line_calculations))


def get_puzzle2(scores):
    total_length = len(scores)
    half_index = int(total_length / 2)
    return sorted(scores)[half_index]


def calculate_ending_line_value(chars):
    values = {
        ')': 1,
        ']': 2,
        '}': 3,
        '>': 4
    }
    start = 0
    for char in reversed(chars):
        start = (start * 5) + values.get(CHAR_COMBINATIONS.get(char))
    return start


def calculate_incorrupt(chars):
    values = {
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137
    }
    return sum([values.get(char) for char in chars])


def get_incorrectly_closed(line):
    queue = deque()
    for index, char in enumerate(line):
        if char in CHAR_COMBINATIONS:
            queue.append(char)
        else:
            starting_char = queue.pop()
            if CLOSED_MAPPING.get(char) != starting_char:
                return False, char
    return True, queue


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    # input_data = TEST
    parsed_data = input_data.splitlines()
    solve_puzzle(parsed_data)