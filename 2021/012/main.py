from collections import Counter
import time


TEST = """3,4,3,1,2"""


DAYS_INTERNAL = 3
TOTAL_DAYS = 80
RESET_LIFECYCLE = 8


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def get_initial_state(data):
    return [int(value) for value in data.split(',')]


def lifecycle(fishes_count, total_days, current_interval_day):
    print('start', total_days, fishes_count)
    if total_days == 0:
        return fishes_count

    new_fish_count = Counter()
    new_fishes_count = 0

    for fish_lifetime, number_of_fishes in fishes_count.items():
        new_fish_lifetime = fish_lifetime - 1
        if new_fish_lifetime < 0:
            new_fish_lifetime = 6
            new_fishes_count += number_of_fishes
        new_fish_count.update({new_fish_lifetime: number_of_fishes})

    current_interval_day -= 1
    if current_interval_day < 0:
        current_interval_day = 6
        if new_fishes_count == 0:
            new_fishes_count += 1

    update = new_fish_count.get(8, 0) + new_fishes_count
    new_fish_count.update({
        8: update
    })

    total_days -= 1
    return lifecycle(new_fish_count, total_days, current_interval_day)


if __name__ == "__main__":
    # input_data = TEST
    input_data = get_input_data('input.txt')

    initial_state = Counter(get_initial_state(input_data))
    print(initial_state)
    start_time = time.time()
    fishes = lifecycle(initial_state, 256, current_interval_day=3)
    # 1590327954513
    print(sum(fishes.values()))
    print(time.time() - start_time)