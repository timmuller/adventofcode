import functools
import itertools


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()

def get_increases(input_data):
    signals = [int(signal) for signal in input_data.splitlines()]
    total_signals = len(signals)

    increases = 0

    for index, current_value in enumerate(signals):
        if index == total_signals - 1:
            break
        next_value = signals[index + 1]
        if next_value >= current_value:
            increases += 1

    return increases

if __name__ == "__main__":
    input_data = get_input_data('input.txt')

    # 1832
    print(get_increases(input_data))