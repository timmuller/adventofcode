
DATA = """
Lightsaber: 13 Batterij, 73 Unobtanium
HandheldComputer: 47 Batterij, 2 Printplaat, 97 Plastic
ElectrischeRacebaan: 43 AutoChassis, 37 Printplaat, 59 Plastic, 41 Batterij, 11 Wiel
QuadDrone: 37 Accu, 2 Plastic, 43 Printplaat
PikachuPlushy: 97 Batterij
Trampoline: 43 Schokdemper, 89 IJzer
BatmobileReplica: 67 BatmobileChassis, 19 Schokdemper, 7 Unobtanium, 7 Wiel
DanceDanceRevolutionMat: 5 Schokdemper, 11 Batterij
Printplaat: 19 Hars, 61 Koper, 61 Chip, 43 Led
Accu: 37 Batterij
Schokdemper: 31 IJzer, 53 Staal
Batterij: 3 Staal
BatmobileChassis: 17 AutoChassis, 13 Staal
AutoChassis: 61 IJzer
Wiel: 7 Rubber, 53 IJzer
Unobtanium: 19 IJzer, 43 Kryptonite
"""

from collections import defaultdict

def attempt_to_solve(missing_components):
    updated_dict = defaultdict(list)
    for key, values in missing_components.items():
        for value in values:
            if isinstance(value, int):
                updated_dict[key].append(value)
                continue
            total, material = value.split(' ')
            if material not in missing_components.keys():
                updated_dict[key].append(int(total))
            else:
                depending_component = missing_components[material]
                try:
                    material_count = sum(depending_component)
                    updated_dict[key].append(material_count * int(total))
                except TypeError:
                    updated_dict[key].append(value)
                    continue
    return updated_dict 

def find_missing_speelgoed(total_components, sum_per_toy):
    left = total_components
    most_to_least = list(reversed(sum_per_toy))
    missing_toys = []
    missing_toys_values = []
    for key, value in most_to_least:
        if value > left:
            continue
        rest = left % value 
        count_toy = (left - rest) / value
        missing_toys += [key] * count_toy
        missing_toys_values += [value] * count_toy
        left = rest
	if rest == 0:
	    return missing_toys
        print(missing_toys)
 
    
    
 

if __name__ == "__main__":
    key_unprocessed = [line.split(':') for line in DATA.strip().splitlines()]
    key_values = dict((key, component_string.strip().split(', ')) for key, component_string in key_unprocessed)

    first_run = attempt_to_solve(key_values)
    print(first_run)
    print()
    print("SECOND RUN")
    second_run = attempt_to_solve(first_run)
    print("THIRD RUN")
    third_run = attempt_to_solve(second_run)
    print(third_run)

    sum_missing = [(key, sum(missing_values)) for key, missing_values in third_run.items()]
    print(sum_missing.sort(key=lambda x: x[1]))
    print(sum_missing)
    
    find_missing_speelgoed(total_components=305333, sum_per_toy=sum_missing)

