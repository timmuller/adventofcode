import functools
import itertools
from collections import defaultdict
from dataclasses import dataclass, field
from typing import List

TEST="""5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526"""
# TEST="""11111
# 19991
# 19191
# 19991
# 11111"""


@dataclass
class Octopus():
    value: int
    x: int
    y: int
    flashes: int = 0
    flashed: bool = False
    related: List['Octopus'] = field(default_factory=list)

    @property
    def id(self):
        return f'{self.y},{self.x}'

    def add_related(self, *relatedes):
        for related in relatedes:
            if related not in self.related:
                self.related.append(related)

    def increase(self, ignore_octo=None):
        if ignore_octo is None:
            ignore_octo = []

        self.value = self.value + 1
        if self.value > 9 and self.flashed is False:
            self.flashed = True
            ignore_octo.append(self.id)
            for related_octo in self.related:
                if related_octo.id in ignore_octo:
                    continue
                related_octo.increase(ignore_octo=ignore_octo)

    def handle_flash(self):
        if self.value > 9:
            self.value = 0
            self.flashes += 1
            self.flashed = False

    def __str__(self):
        return f"Octo - {self.value} - {len(self.related)} ({self.y}, {self.x})"


@dataclass
class Map():
    octos = defaultdict(dict)

    def list(self):
        for lines in self.octos.values():
            for octo in lines.values():
                yield octo

    def tick(self):
        for octo in self.list():
            octo.increase()
        for octo in self.list():
            octo.handle_flash()

    def add_octo(self, octo):
        related_octos = []
        for y in range(octo.y-1, octo.y+2):
            for x in range(octo.x-1, octo.x+2):
                related_octo = self.octos.get(y, {}).get(x)
                if related_octo:
                    related_octos.append(related_octo)

        octo.add_related(*related_octos)

        for related_octo in related_octos:
            related_octo.add_related(octo)
        self.octos[octo.y][octo.x] = octo

    def stats(self):
        flashes = 0
        for octo in self.list():
            flashes += octo.flashes
        return flashes

    def __str__(self):
        string = ""
        for lines in self.octos.values():
            for octo in lines.values():
                string += str(octo.value) + " "
            string += "\n"
        return string


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def setup_chains(input_data):
    octo_map = Map()
    for y, line in enumerate(input_data.splitlines()):
        for x, value in enumerate(line):
            octo = Octopus(
                value=int(value),
                x=x,
                y=y,
            )
            octo_map.add_octo(octo)

    return octo_map


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    # input_data = TEST

    octomap = setup_chains(input_data)
    for tick in range(100):
        octomap.tick()
        print("tick {} = flash count {}".format(tick+1, octomap.stats()))
    print(octomap)

    # 1617