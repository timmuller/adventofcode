from collections import Counter
from itertools import combinations, product

TEST = """3,4,3,1,2"""


DAYS_INTERNAL = 3
TOTAL_DAYS = 80
RESET_LIFECYCLE = 8


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()

def get_initial_state(data):
    return [int(value) for value in data.split(',')]



def lifecycle(fishes, total_days, current_interval_day):
    if total_days == 0:
        return fishes

    new_lifecycle_fishes = []
    new_fishes = []

    for fish in fishes:
        fish_lifecycle = fish - 1
        if fish_lifecycle < 0:
            fish_lifecycle = 6
            new_fishes.append(RESET_LIFECYCLE)
        new_lifecycle_fishes.append(fish_lifecycle)

    current_interval_day -= 1
    if current_interval_day < 0:
        current_interval_day = 6
        if len(new_fishes) == 0:
            new_fishes.append(RESET_LIFECYCLE)


    new_lifecycle_fishes += new_fishes
    total_days -= 1
    # print(total_days, current_interval_day, new_lifecycle_fishes)
    return lifecycle(new_lifecycle_fishes, total_days, current_interval_day)


if __name__ == "__main__":
    input_data = TEST
    # input_data = get_input_data('input.txt')

    initial_state = get_initial_state(input_data)

    initial_day = DAYS_INTERNAL
    fishes = lifecycle(initial_state, TOTAL_DAYS, current_interval_day=3)
    # 350149
    print(len(fishes))