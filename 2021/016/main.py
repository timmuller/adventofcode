from collections import Counter, defaultdict
import time


TEST = """be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"""
# TEST = """acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"""


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()

def convert_to_digit(digit_sequence):
    digit_sequence.split().inter


def determine_signal(line):
    first_digit_output, four_digit_output = line.split('|')
    return first_digit_output.strip().split(' '), four_digit_output.strip().split(' ')


def get_all(input_data):
    for line in input_data.splitlines():
        yield determine_signal(line)


def get_number(first_part, second_part):
    all_parts = first_part + second_part
    unique_parts = set(["".join(sorted(part)) for part in all_parts])
    return solve_wires(unique_parts)


def solve_wires(all_parts):
    solved, unsolved_parts = solve_by_length(all_parts)
    solved, _ = deduction(solved, unsolved_parts)
    return solved


def deduction(solved, unsolved):
    # 6 9 0
    length_6 = [part for part in unsolved if len(part) == 6]
    solved = solve_simalar_length6(solved, length_6)

    # 2 3 5
    length_5 = [part for part in unsolved if len(part) == 5]
    solved = solve_simalar_length5(solved, length_5)


    unsolved = _recalculate_to_solve(solved, unsolved)
    return solved, unsolved


def _recalculate_to_solve(solved, unsolved):
    return list(set(unsolved) - set(solved.keys()))


def solve_simalar_length5(solved, to_solve):
    one_range = _get_by_number(1, solved)
    three_matches = [current for current in to_solve if _is_part_of(current, one_range)]
    if len(three_matches) == 1:
        solved[three_matches[0]] = 3
    to_solve = _recalculate_to_solve(solved, to_solve)

    nine_range = _get_by_number(9, solved)
    five_matches = [current for current in to_solve if _count_left(current, nine_range, 0)]
    if len(five_matches) == 1:
        solved[five_matches[0]] = 5
    to_solve = _recalculate_to_solve(solved, to_solve)

    if len(to_solve) == 0:
        return solved
    if len(to_solve) == 1:
        solved[to_solve[0]] = 2
        return solved
    raise RuntimeError("To many items left")


def solve_simalar_length6(solved, to_solve):
    zeven_range = _get_by_number(7, solved)
    six_matches = [current for current in to_solve if not _is_part_of(current, zeven_range)]
    if len(six_matches) == 1:
        solved[six_matches[0]] = 6

    to_solve = _recalculate_to_solve(solved, to_solve)
    four_range = _get_by_number(4, solved)
    nine_matches = [current for current in to_solve if _is_part_of(current, four_range)]
    if len(nine_matches) == 1:
        solved[nine_matches[0]] = 9

    to_solve = _recalculate_to_solve(solved, to_solve)
    if len(to_solve) == 0:
        return solved
    if len(to_solve) == 1:
        solved[to_solve[0]] = 0
        return solved
    raise RuntimeError("To many items left")


def _count_left(current, compare_with, unique_length):
    left = set(current) - set(compare_with)
    if len(left) == unique_length:
        return True
    return False


def _part_of(solving, include_parts):
    match = []
    for current in solving:
        intersection = set(current).intersection(include_parts)
        if len(intersection) == len(include_parts):
            match.append(current)
    if len(match) == 0:
        return
    if len(match) == 1:
        return match[0]
    raise RuntimeError('found too many matches')


def _is_part_of(current, include_parts):
    intersection = set(current).intersection(include_parts)
    return len(intersection) == len(include_parts)


def _get_by_number(number, solved):
    for key, value in solved.items():
        if value == number:
            return key


def solve_by_length(all_parts):
    length_to_digit = {
        2: 1,
        3: 7,
        4: 4,
        7: 8,
    }

    solved = {}
    unsolved_parts = []
    for part in all_parts:
        digit_by_length = length_to_digit.get(len(part))
        if digit_by_length:
            solved[part] = digit_by_length
        else:
            unsolved_parts.append(part)
    return solved, unsolved_parts


def get_four_digit_answer(second_parts, numbers):
    solved = ''
    for part in second_parts:
        sorted_part = "".join(sorted(part))
        solved += str(numbers[sorted_part])
    return solved


if __name__ == "__main__":
    # input_data = TEST
    input_data = get_input_data('input.txt')

    calculated_numbers = []
    for first_part, second_part in get_all(input_data):
        numbers = get_number(first_part, second_part)
        calculated_numbers.append(get_four_digit_answer(second_part, numbers))

    print(sum(int(value) for value in calculated_numbers))

    # # 369
    # print(len([True for signal in get_all(input_data) if len(signal) in length_to_digit]))

