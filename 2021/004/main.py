import functools
import itertools
from collections import defaultdict
TEST="""
forward 5
down 5
forward 8
up 3
down 8
forward 2
"""


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def parse_data(input_data):
    for line in input_data.splitlines():
        yield tuple(line.split(' '))


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    parsed_key_value = parse_data(input_data)
    sum_per_movement = defaultdict(int)

    horizontal = 0
    aim = 0
    depth = 0
    for movement, paces in parsed_key_value:
        paces = int(paces)
        if movement == 'forward':
            horizontal += paces
            depth += aim * paces
        if movement == 'up':
            #depth -= paces
            aim -= paces
        if movement == 'down':
            #depth += paces
            aim += paces
   
    # 1965970888 = answer 
    print(horizontal, depth, horizontal * depth, aim)

