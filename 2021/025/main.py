import functools
import itertools

TEST="""
6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5
"""



def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    input_data = TEST