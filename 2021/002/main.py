import functools
import itertools


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()

def get_increases(signals):
    total_signals = len(signals)

    increases = 0

    for index, current_value in enumerate(signals):
        if index == total_signals - 1:
            break
        next_value = signals[index + 1]
        if next_value > current_value:
            increases += 1

    return increases


def get_slices(signals):
    try:
        for index, current_value in enumerate(signals):
            second_value, third_value = signals[index+1], signals[index+2]

            yield current_value + second_value + third_value
    except (ValueError, IndexError):
        return StopIteration


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    signals = [int(signal) for signal in input_data.splitlines()]

    signal_slice_counts = list(get_slices(signals))
    # 1858
    print(get_increases(signal_slice_counts))