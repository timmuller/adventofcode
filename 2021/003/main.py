import functools
import itertools
from collections import defaultdict
TEST="""
forward 5
down 5
forward 8
up 3
down 8
forward 2
"""


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def parse_data(input_data):
    for line in input_data.splitlines():
        yield tuple(line.split(' '))


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    parsed_key_value = parse_data(input_data)
    sum_per_movement = defaultdict(int)
    for movement, paces in parsed_key_value:
        sum_per_movement[movement] += int(paces)

    horizontal = sum_per_movement['forward']
    depth = sum_per_movement['down'] - sum_per_movement['up']

    # (1957, 955, 1868935)
    print(horizontal, depth, horizontal * depth)

