import functools
import itertools
from collections import defaultdict, Counter

TEST="""00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"""


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def parse_data(input_data):
    for line in input_data.splitlines():
        yield tuple(line.split(' '))


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    # input_data = TEST

    streams = defaultdict(str)

    for line in input_data.splitlines():
        for position, number in enumerate(line):
            streams[position] += number


    common_numbers = ""
    reversed_numbers = ""
    for value in streams.values():
        counted = Counter(value)
        common_number, = counted.most_common(1)
        highest_number, count = common_number
        common_numbers += highest_number
        if highest_number == "0":
            reversed_numbers += "1"
        else:
            reversed_numbers += "0"

    print(common_numbers, reversed_numbers)
    print(int(common_numbers, 2) * int(reversed_numbers, 2))
    # answer 2640986