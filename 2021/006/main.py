import functools
import itertools
from collections import defaultdict, Counter

TEST="""00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"""


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def parse_data(input_data):
    for line in input_data.splitlines():
        yield tuple(line.split(' '))


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    # input_data = TEST

    streams = defaultdict(str)

    for line in input_data.splitlines():
        for position, number in enumerate(line):
            streams[position] += number


    gamma_bits = ""
    epsilon_bits = ""
    for value in streams.values():
        counted = Counter(value)
        common_number, = counted.most_common(1)
        highest_number, count = common_number
        gamma_bits += highest_number
        if highest_number == "0":
            epsilon_bits += "1"
        else:
            epsilon_bits += "0"

    gamme = int(gamma_bits, 2)
    epsilon = int(epsilon_bits, 2)
    # print(gamma_bits, epsilon_bits)
    # print(int(gamma_bits, 2) * int(epsilon_bits, 2))
    # answer 2640986

    def oxygen_filter(search_input_data, position=0, most=True):
        stream = "".join([value[position] for value in search_input_data])
        common_order = Counter(stream).most_common()

        if len(set(count for pos, count in common_order)) == 1:
            if most:
                determinator = '1'
            else:
                determinator = '0'
        elif most is True:
            determinator = common_order[0][0]
        else:
            determinator = common_order[-1][0]

        search_input = [line for line in search_input_data if line[position] == determinator]
        if len(search_input) == 1:
             return search_input[0]
        return oxygen_filter(search_input, position + 1, most=most)

    oxygen_generator_rating_bit = oxygen_filter(input_data.splitlines())
    oxygen_generator_rating = int(oxygen_generator_rating_bit, 2)
    co2_bit = oxygen_filter(input_data.splitlines(), most=False)
    co2_rating = int(co2_bit, 2)
    print('co2', co2_rating)

    print("oxygen", oxygen_generator_rating, 'co2', co2_rating)
    life_support_rating = oxygen_generator_rating * co2_rating
    print("life support rating", life_support_rating)
    # 6822109