from collections import Counter, defaultdict
import time


TEST = """16,1,2,0,4,2,7,1,2,14"""


DAYS_INTERNAL = 3
TOTAL_DAYS = 80
RESET_LIFECYCLE = 8


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def get_horizontal_numbers(data):
    return [int(value) for value in data.split(',')]


def get_lowest_fuel(positions):
    sorted_positions = sorted(positions.items(), key=lambda x: x[1])
    return sorted_positions[0]


def calculate_fuelcosts(horizontal, suggested):
    steps = abs(horizontal_position - suggested_position)
    return sum(range(1, steps + 1))


if __name__ == "__main__":
    # input_data = TEST
    input_data = get_input_data('input.txt')
    horizontal_positions = get_horizontal_numbers(input_data)

    position_range = list(range(max(horizontal_positions)))

    positions = defaultdict(int)

    print(len(horizontal_positions))
    for index, horizontal_position in enumerate(horizontal_positions):
        for suggested_position in position_range:
            fuel_costs = calculate_fuelcosts(horizontal_position, suggested_position)
            positions[suggested_position] += fuel_costs

        print(index)


    # position 459 costs 86397080
    position, fuel_costs = get_lowest_fuel(positions)
    print(f"position {position} costs {fuel_costs}")