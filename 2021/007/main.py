import functools
import itertools
from collections import defaultdict, Counter

TEST = """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"""



class Board():
    def __init__(self):
        self.rows = []

    @property
    def columns(self):
        first_row_length = len(self.rows[0])
        for position in range(first_row_length):
            yield [row[position] for row in self.rows]

    def add_line(self, line):
        self.rows.append([value for value in line.split(' ') if value != ""])

    def mark(self, number):
        new_rows = []
        for row in self.rows:
            new_row = [value if str(value) != str(number) else None for value in row]
            new_rows.append(new_row)
        self.rows = new_rows

    def is_bingo(self):
        for row in self.rows:
            if all(value is None for value in row):
                return True
        for column in self.columns:
            if all(value is None for value in column):
                return True
        return False

    def get_score(self):
        score_numbers = []
        for row in self.rows:
            for value in row:
                if value is not None:
                    score_numbers.append(int(value))
        return sum(score_numbers)


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def parse_data(input_data):
    bingo_numbers = None
    boards = []
    current_board = None
    for index, line in enumerate(input_data.splitlines()):
        if index == 0:
            bingo_numbers = line.split(',')
            continue
        if line == "":
            if current_board:
                boards.append(current_board)
            current_board = Board()
            continue
        current_board.add_line(line)
    boards.append(current_board)
    return bingo_numbers, boards


def find_bingo_card(bingo_numbers, boards):
    drawn_numbers = []
    for index, bingo_number in enumerate(bingo_numbers):
        drawn_numbers.append(int(bingo_number))
        for board in boards:
            board.mark(bingo_number)
            if board.is_bingo():
                return board, drawn_numbers


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    # input_data = TEST

    bingo_numbers, boards = parse_data(input_data)

    board, bingo_numbers = find_bingo_card(bingo_numbers, boards)

    # 69579
    print(board.get_score() * bingo_numbers[-1])


