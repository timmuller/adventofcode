import functools
import itertools
from dataclasses import dataclass, field
from typing import List

TEST="""start-A
start-b
A-c
A-b
b-d
A-end
b-end"""


nodes = {}

@dataclass
class Node:
    name: str
    leafs: List['Node'] = field(default_factory=list)

    def add_leaf(self, node):
        self.leafs.append(node)

    def __str__(self):
        return "{} {}".format(self.name, [node.name for node in self.leafs])


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()

def build_tree(input_data):
    for line in input_data.splitlines():
        source, destination = line.split('-', maxsplit=1)
        source_node = nodes.get(source, Node(name=source))
        destination_node = nodes.get(destination, Node(name=destination))
        source_node.add_leaf(destination_node)
        nodes.update({
            source: source_node,
            destination: destination_node,
        })

    return nodes['start']



if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    input_data = TEST

    start_node = build_tree(input_data)
    print(start_node)