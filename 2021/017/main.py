import functools
import itertools
from collections import defaultdict
from dataclasses import dataclass, field
from typing import List

TEST="""2199943210
3987894921
9856789892
8767896789
9899965678"""

@dataclass
class HeightPoint:
    x: int
    y: int
    value: int
    lowest: bool = False
    higher: bool = False
    surrounding: List['HeightPoint'] = field(default_factory=list)

    def add_surrounding(self, *heightpoints):
        for heightpoint in heightpoints:
            if heightpoint not in self.surrounding:
                self.surrounding.append(heightpoint)

    def is_open(self):
        return self.lowest is False and self.higher is False

    def __str__(self):
        return f"({self.x}, {self.y}) - value {self.value} - surroundings {len(self.surrounding)}"


class HeightMap():
    _lookup = []
    heightpoints: List[HeightPoint] = []

    def add_point(self, new_heightpoint: HeightPoint):
        self.heightpoints.append(new_heightpoint)

        surrounding_y = [new_heightpoint.y - 1, new_heightpoint.y + 1]
        surrounding_x = [new_heightpoint.x - 1, new_heightpoint.x + 1]
        surrounding_heighpoints = [
            current_heightpoint for current_heightpoint in self.heightpoints
            if (
                (current_heightpoint.y == new_heightpoint.y and current_heightpoint.x in surrounding_x) or
                (current_heightpoint.x == new_heightpoint.x and current_heightpoint.y in surrounding_y)
            )
        ]
        new_heightpoint.add_surrounding(*surrounding_heighpoints)
        for surrounding_heighpoint in surrounding_heighpoints:
            surrounding_heighpoint.add_surrounding(new_heightpoint)

    def find_by_value(self, value):
        for heightpoint in self.heightpoints:
            if not heightpoint.is_open():
                continue
            if heightpoint.value == value:
                yield heightpoint

    def get_lowest_points(self):
        for heightpoint in self.heightpoints:
            if heightpoint.lowest:
                yield heightpoint

    def __str__(self):
        overview_map = defaultdict(dict)
        for point in self.heightpoints:
            marker = '.'
            if point.higher:
                marker = 'H'
            if point.lowest:
                marker = str(point.value)
            overview_map[point.y][point.x] = marker

        string = ""
        for _, points in overview_map.items():
            for _, point in points.items():
                string += point
            string +="\n"
        return string


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()


def parse_height_map(input_data):
    height_map = HeightMap()
    for y, line in enumerate(input_data.splitlines()):
        for x, value in enumerate(line):
            height_map.add_point(
                HeightPoint(
                    x=x,
                    y=y,
                    value=int(value)
                )
            )
    return height_map


def solve_lowest_points(heightmap: HeightPoint):
    for find_height in range(10):
        mark_by_height(find_height, heightmap)


def mark_by_height(find_height, heightmap):
    for heightpoint in heightmap.find_by_value(find_height):
        heightpoint.lowest = True
        remove_higher_points(heightpoint)


def remove_higher_points(heightpoint: HeightPoint):
    for sur_heighpoint in heightpoint.surrounding:
        if not sur_heighpoint.is_open():
            continue
        if sur_heighpoint.value >= heightpoint.value:
            sur_heighpoint.higher = True
            remove_higher_points(sur_heighpoint)


if __name__ == "__main__":
    input_data = get_input_data('input.txt')
    # input_data = TEST

    height_map = parse_height_map(input_data)
    solve_lowest_points(height_map)
    lowest_points = height_map.get_lowest_points()
    risk_levels = [point.value + 1 for point in lowest_points]

    # 514
    print(sum(risk_levels))