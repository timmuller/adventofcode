import { loadPuzzle } from "../utils";
import { puzzle1, puzzle2 } from "./day4";

const PUZZLE_NUMBER = '4'
const PUZZLE_FOLDER = `2022_${PUZZLE_NUMBER}`

describe(`Day ${PUZZLE_NUMBER}`, () => {
    describe("Puzzle1", () => {
        it('example', () => {
            const input = `6-6,4-6`

            expect(puzzle1(input)).toEqual(1)
        })

        it('example', () => {
            const input = `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`

            expect(puzzle1(input)).toEqual(2)
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle1(content)).toBe(550);
        })
    })
    describe.only("Puzzle2", () => {
        it('example', () => {
            const input = `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`

            expect(puzzle2(input)).toEqual(4)
        })


        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle2(content)).toBe(931)
        })
    });
})

