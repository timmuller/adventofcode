import { loadPuzzle } from "../utils";
import { puzzle1, puzzle2 } from "./day1";

const PUZZLE_NUMBER = '1'
const PUZZLE_FOLDER = `2022_${PUZZLE_NUMBER}`

describe(`Day ${PUZZLE_NUMBER}`, () => {
    describe("Puzzle1", () => {

        it('example', () => {
            const input = `1000
2000
3000

4000

5000
6000

7000
8000
9000

10000`   
            
            expect(puzzle1(input)).toEqual(24000)
        })

        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle1(content)).toBe(70369);
        })
    })
    describe("Puzzle2", () => {
        it('example', () => {
            const input = `1000
2000
3000

4000

5000
6000

7000
8000
9000

10000`   
            
            expect(puzzle2(input)).toEqual(45000)
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle2(content)).toBe(203002)
        })
    
    });
})

