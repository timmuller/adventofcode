import { loadPuzzle } from "../utils";
import { puzzle1, puzzle2 } from "./day2";

const PUZZLE_NUMBER = '2'
const PUZZLE_FOLDER = `2022_${PUZZLE_NUMBER}`

describe(`Day ${PUZZLE_NUMBER}`, () => {
    describe("Puzzle1", () => {
        it.each([
            {input: 'A Y', expected: 8},
            {input: 'B X', expected: 1},
            {input: 'C Z', expected: 6},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(puzzle1(input)).toBe(expected);
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle1(content)).toBe(10941);
        })
    })
    describe("Puzzle2", () => {
        it.each([
            {input: 'A Y', expected: 4},
            {input: 'B X', expected: 1},
            {input: 'C Z', expected: 7},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(puzzle2(input)).toBe(expected);
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle2(content)).toBe(13071)
        })
    });
})

