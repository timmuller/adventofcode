interface ElvCaloriesProcessing {
    currentElv: Array<number>
    previousElves: Array<number>
}


function groupPerElv(lines: String[]): ElvCaloriesProcessing {
    const groupedPerElves = lines.reduce((previousValue, currentValue) => {
        if (currentValue === '') {
            const calories: Array<number> = previousValue.currentElv 
            previousValue.previousElves.push(calories.reduce((a, b) => a + b, 0))
            previousValue.currentElv = []
        } else {
            previousValue.currentElv.push(+currentValue)
        }
        
        return previousValue
    }, {currentElv: [], previousElves: []} as ElvCaloriesProcessing)

    const calories: Array<number> = groupedPerElves.currentElv 
    groupedPerElves.previousElves.push(calories.reduce((a, b) => a + b, 0))
    groupedPerElves.currentElv = []
    return groupedPerElves
}

export function puzzle1(content: String): number {
    const lines = content.split('\n')
    const groupedPerElv: ElvCaloriesProcessing = groupPerElv(lines)
    const highestValue = groupedPerElv.previousElves.sort((a,b) => b - a)[0]
    return highestValue
}

export function puzzle2(content: String) : number{
    const lines = content.split('\n')
    const groupedPerElv: ElvCaloriesProcessing = groupPerElv(lines)
    const highestValues = groupedPerElv.previousElves.sort((a,b) => b - a)
    return highestValues.slice(0, 3).reduce((a, b) => a + b, 0)
}
