
function parseStartingStack(input: string): Array<Array<String>> {
    const reversedStack = input.split("\n").reverse()

    const firstLine = reversedStack.shift() ?? ""
    const stacks: Array<Array<string>> = firstLine.split("   ").map(_ => [])

    reversedStack.forEach((currentLine) => {
        stacks.forEach((value, index) => {
            const indexLookup = ((index + 1) * 4) - 3
            const letter = currentLine[indexLookup < 0 ? 1 : indexLookup] as string
            if(index === 0){
            console.log(index, letter, indexLookup, currentLine)
            }
            if (letter && letter !== ' '){
                stacks[index].push(letter)
            }
        })
    })

    return stacks
}

function parseStep(step: String) {
    const [_1, numberOfCrates, _2, fromStackIndex, _3, toStackIndex] = step.split(' ')
    return [parseInt(fromStackIndex) - 1, parseInt(toStackIndex) - 1, parseInt(numberOfCrates)]
}


function moveStack(stack: Array<Array<String>>, step: String){
        const [fromStack, toStack, numberOfItems] = parseStep(step);
        if (!numberOfItems){
            return stack
        }

        [...new Array(numberOfItems).keys()].forEach(() => {
            const itemToMove: String | undefined = stack[fromStack].pop()
            if (itemToMove) {
                stack[toStack].push(itemToMove)
            }
        })

        return stack
}

function moveStack9001(stack: Array<Array<String>>, step: String){
        const [fromStack, toStack, numberOfItems] = parseStep(step);
        if (!numberOfItems){
            return stack
        }

        const toMove: Array<String> = [];
        [...new Array(numberOfItems).keys()].forEach(() => {
            const itemToMove: String | undefined = stack[fromStack].pop()
            if (itemToMove) {
                toMove.push(itemToMove)
            }
        })
        toMove.reverse().forEach(item => stack[toStack].push(item))
        return stack
}

function getStackAnswer(stacks: Array<Array<String>>){
    let answer = ""
    stacks.forEach(stack => answer += stack.pop())
    return answer
}

export function puzzle1(content: String){
    const [startStack, steps] = content.split('\n\n')

    let stack = parseStartingStack(startStack)
    steps.split('\n').forEach(step => {
        stack = moveStack(stack, step)
    })

    return getStackAnswer(stack)

}

export function puzzle2(content: String){
    const [startStack, steps] = content.split('\n\n')

    let stack = parseStartingStack(startStack)
    steps.split('\n').forEach(step => {
        stack = moveStack9001(stack, step)
    })

    return getStackAnswer(stack)
}
