

function fullContaining(range1: Number[], range2: Number[]): Boolean {

    const range1Filter = range1.filter(x => !range2.includes(x))
    const range2Filter = range2.filter(x => !range1.includes(x))
    return !range1Filter.length || !range2Filter.length
}

function anyOverlappingContaining(range1: Number[], range2: Number[]): Boolean {
    const range1Filter = range1.filter(x => range2.includes(x))
    const range2Filter = range2.filter(x => range1.includes(x))
    return range1Filter.length != 0 || range2Filter.length != 0
}

function GeneratePositions(position: String): Number[] {
    const [start, end] = position.split('-')
    const startInt = parseInt(start)
    const endInt = parseInt(end) + 1

    return Array.from(new Array(endInt - startInt).keys()).map(x => x + startInt);

}

export function puzzle1(content: String){
    const lines = content.split('\n')

    return lines.reduce((previousCount, currentrucksack) => {
        const [string1, string2] = currentrucksack.split(',');

        if (fullContaining(GeneratePositions(string1), GeneratePositions(string2))){
            return previousCount + 1
        }
        return previousCount
    }, 0)

}

export function puzzle2(content: String){
    const lines = content.split('\n')

    return lines.reduce((previousCount, currentrucksack) => {
        const [string1, string2] = currentrucksack.split(',');

        if (anyOverlappingContaining(GeneratePositions(string1), GeneratePositions(string2))){
            return previousCount + 1
        }
        return previousCount
    }, 0)
}
