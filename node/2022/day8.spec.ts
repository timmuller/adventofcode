import { loadPuzzle } from "../utils";
import { puzzle1, puzzle2 } from "./day8";

const PUZZLE_NUMBER = '8'
const PUZZLE_FOLDER = `2022_${PUZZLE_NUMBER}`

describe(`Day ${PUZZLE_NUMBER}`, () => {
    describe("Puzzle1", () => {

      it('test example', () => {
        const input = `30373
25512
65332
33549
35390`
        expect(puzzle1(input)).toEqual(21)
      })

//         it.each([
//             {input: 'vJrwpWtwJgWrhcsFMMfFFhFp', expected: 16},
//         ])("test $input to be $expected", ({input, expected}) => {
//             expect(puzzle1(input)).toBe(expected);
//         })

//         it("test puzzle using input", () => {
//             const content: String = loadPuzzle(PUZZLE_FOLDER)
//             expect(puzzle1(content)).toBe("MQSHJMWNH");
//         })
    })
//     describe("Puzzle2", () => {
//         it("test puzzle using input", () => {
//             const content: String = loadPuzzle(PUZZLE_FOLDER)
//             expect(puzzle2(content)).toBe("LLWJRBHVZ")
//         })
//     });
})