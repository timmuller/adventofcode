enum ObjectTypes {
    FILE = "FILE",
    DIR = "DIR",
}


interface FileSystemNode {
    parent_node: FileSystemNode | null,
    child_nodes: Array<FileSystemNode>
    type: ObjectTypes,
    size: number[],
    name: string,
}

function getSize(node: FileSystemNode) : number {
    return node.size.reduce((prev, cur) => {return prev + cur}, 0)
}

function recalculateSize(node: FileSystemNode, size: number): void{
    node.size.push(size)
    if(node.parent_node){
        return recalculateSize(node.parent_node, size)
    }
    return;
}

function backToRoot(node: FileSystemNode) : FileSystemNode{
    if(node.parent_node === null){
        return node
    } else {
        return backToRoot(node.parent_node)
    }
}

function executeCommand(node: FileSystemNode, command: string) : FileSystemNode {
    const commandParts  = command.split(' ')
    const startsWithDollar = commandParts[0] == '$'
    const exec = startsWithDollar? commandParts[1] : commandParts[0]
    const args = (startsWithDollar ? commandParts[2] : commandParts[1]) ?? null

    if (exec == 'cd') {
        if (args == '/') {
            return backToRoot(node)
        }
        if (args == '..') {
            return node.parent_node ?? node
        } else {
            return node.child_nodes.find(node => node.name == args) ?? node
        }
    }
    if (exec == 'ls'){
        return node
    }
    if (exec == 'dir'){
        const dirNode: FileSystemNode  = {
            parent_node: node,
            child_nodes: [],
            type: ObjectTypes.DIR,
            size: [],
            name: args
        }
        node.child_nodes.push(dirNode)
        return node
    }
    if (parseInt(exec)){
        const size = parseInt(exec)
        const fileNode: FileSystemNode  = {
            parent_node: node,
            child_nodes: [],
            type: ObjectTypes.FILE,
            size: [size],
            name: args
        }
        node.child_nodes.push(fileNode)
        recalculateSize(node, size)
        return node
    }
    throw new Error(command + " - " + exec + " - " + args + " - " + commandParts)
}

function findCleanupDirs(node: FileSystemNode, maxSize: number): FileSystemNode[] {
    let matchDirs = node.child_nodes.filter(childNode => {
        return childNode.type === ObjectTypes.DIR && getSize(childNode) <= maxSize
    })

    node.child_nodes.forEach((childNode => {
        const matchingChildDirs = findCleanupDirs(childNode, maxSize)
        if(matchingChildDirs){
            matchDirs = matchDirs.concat(matchingChildDirs)
        }
    }))

    return matchDirs
}

function findEffectiveDirs(node: FileSystemNode, toFreeUp: number) : FileSystemNode[] {
    let matchDirs = node.child_nodes.filter(childNode => {
        return childNode.type === ObjectTypes.DIR && getSize(childNode) >= toFreeUp
    })
    node.child_nodes.forEach((childNode => {
        const matchingChildDirs = findEffectiveDirs(childNode, toFreeUp)
        if(matchingChildDirs){
            matchDirs = matchDirs.concat(matchingChildDirs)
        }
    }))

    return matchDirs
}

export function puzzle1(content: String){
    const lines = content.split('\n')

    const rootTree: FileSystemNode  = {
        parent_node: null,
        child_nodes: [],
        type: ObjectTypes.DIR,
        size: [],
        name: '/'
    }

    lines.reduce((currentNode, command) => {
        return executeCommand(currentNode, command)
    }, rootTree)

    const directories: FileSystemNode[] = findCleanupDirs(rootTree, 100000)
    const size = directories.reduce((prevValue, curDir) => {return prevValue + getSize(curDir)}, 0)
    return size
}

export function puzzle2(content: String){
    const lines = content.split('\n')
    const rootTree: FileSystemNode  = {
        parent_node: null,
        child_nodes: [],
        type: ObjectTypes.DIR,
        size: [],
        name: '/'
    }

    lines.reduce((currentNode, command) => {
        return executeCommand(currentNode, command)
    }, rootTree)

    const totalsize = 70000000
    const requiredSize = 30000000
    const totalDirSize = getSize(rootTree)
    const needToFreeSize = requiredSize - (totalsize - totalDirSize)

    const dir = findEffectiveDirs(rootTree, needToFreeSize)
    const sortedDir = dir.sort((a, b) => (getSize(a) - getSize(b)))

    return getSize(sortedDir[0])
}
