
enum Shape {
    ROCK,
    PAPER,
    SCISSORS,
}

enum Result {
    WIN,
    LOST,
    DRAW
}

interface HandCodes {
    rock: String
    paper: String
    scissors: String
}

const opponentHand: HandCodes = {
    rock: 'A',
    paper: 'B',
    scissors: 'C',
}

const myHand: HandCodes = {
    rock: 'X',
    paper: "Y",
    scissors: "Z"
}

const ShapeWinsOnShape: Map<Shape, Shape> = new Map([
    [Shape.ROCK, Shape.SCISSORS],
    [Shape.PAPER, Shape.ROCK],
    [Shape.SCISSORS, Shape.PAPER]
])

const ScoresPerShape: Map<Shape, number> =  new Map([
    [Shape.ROCK, 1],
    [Shape.PAPER, 2],
    [Shape.SCISSORS, 3],
])

const ResultScore: Map<Result, number> = new Map([
    [Result.WIN, 6],
    [Result.DRAW, 3],
    [Result.LOST, 0],
])

function getMove(hand: HandCodes, move: String): Shape {
    switch(move) {
        case hand.rock:
            return Shape.ROCK
        case hand.paper:
            return Shape.PAPER
        case hand.scissors:
            return Shape.SCISSORS
    }
    throw new Error('Shape not found')
}


function concludeHand(opponentMove: Shape, myMove: Shape): Result {
    if (ShapeWinsOnShape.get(myMove) === opponentMove){
        return Result.WIN
    }
    if (opponentMove === myMove) {
        return Result.DRAW
    }

    return Result.LOST
}

function changeMove(opponentMove: Shape, myMove: Shape): Shape | undefined {
    if (myMove == Shape.ROCK){
        return ShapeWinsOnShape.get(opponentMove)
    }
    if (myMove == Shape.PAPER) {
        return opponentMove
    }

    const winningMoveCombo = Array.from(ShapeWinsOnShape.entries()).find(([winningMove, losingMove]) => {
        return losingMove == opponentMove
    })
    if (winningMoveCombo) {
        return winningMoveCombo[0]
    }
}



export function puzzle1(content: String){
    const lines = content.split('\n')
    return lines.reduce((previousScore, currentMove) => {
        const [opponentMoveCode, myMoveCode] = currentMove.split(' ');
        const opponentMove = getMove(opponentHand, opponentMoveCode);
        const myMove = getMove(myHand, myMoveCode);

        const myResult: Result = concludeHand(opponentMove, myMove)
        let myAdditionalScore = ScoresPerShape.get(myMove) ?? 0
        myAdditionalScore += ResultScore.get(myResult) ?? 0

        return previousScore + myAdditionalScore
    }, 0)
}

export function puzzle2(content: String){
    const lines = content.split('\n')
    return lines.reduce((previousScore, currentMove) => {
        const [opponentMoveCode, myMoveCode] = currentMove.split(' ');
        const opponentMove = getMove(opponentHand, opponentMoveCode);
        const myMove = getMove(myHand, myMoveCode);

        const myChangedMove = changeMove(opponentMove, myMove)
        if (myChangedMove === undefined) {
            throw new Error('undefined move')
        }
        const myResult: Result = concludeHand(opponentMove, myChangedMove)
        let myAdditionalScore = ScoresPerShape.get(myChangedMove) ?? 0
        myAdditionalScore += ResultScore.get(myResult) ?? 0

        return previousScore + myAdditionalScore
    }, 0)
}
