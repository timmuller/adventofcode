import { loadPuzzle } from "../utils";
import { puzzle1, puzzle2 } from "./day5";

const PUZZLE_NUMBER = '5'
const PUZZLE_FOLDER = `2022_${PUZZLE_NUMBER}`

describe(`Day ${PUZZLE_NUMBER}`, () => {
    describe("Puzzle1", () => {

        it('example', () => {
            const input = `
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
`

            expect(puzzle1(input)).toEqual("CMZ")
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle1(content)).toBe("MQSHJMWNH");
        })
    })
    describe("Puzzle2", () => {
        it('example', () => {
            const input = `
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
`

            expect(puzzle2(input)).toEqual("MCD")
        })


        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle2(content)).toBe("LLWJRBHVZ")
        })
    });
})

