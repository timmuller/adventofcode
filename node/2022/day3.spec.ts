import { loadPuzzle } from "../utils";
import { puzzle1, puzzle2 } from "./day3";

const PUZZLE_NUMBER = '3'
const PUZZLE_FOLDER = `2022_${PUZZLE_NUMBER}`

describe(`Day ${PUZZLE_NUMBER}`, () => {
    describe("Puzzle1", () => {

        it('example', () => {
            const input = `vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`

            expect(puzzle1(input)).toEqual(157)
        })

        it.each([
            {input: 'vJrwpWtwJgWrhcsFMMfFFhFp', expected: 16},
            {input: 'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL', expected: 38},
            {input: 'PmmdzqPrVvPwwTWBwg', expected: 42},
            {input: 'wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn', expected: 22},
            {input: 'ttgJtRGJQctTZtZT', expected: 20},
            {input: 'CrZsJsPPZsGzwwsLwLmpwMDw', expected: 19},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(puzzle1(input)).toBe(expected);
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle1(content)).toBe(7581);
        })
    })
    describe("Puzzle2", () => {
        it('example', () => {
            const input = `vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg`

            expect(puzzle2(input)).toEqual(18)
        })

        it('example', () => {
            const input = `vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`

            expect(puzzle2(input)).toEqual(70)
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle2(content)).toBe(2525)
        })
    });
})

