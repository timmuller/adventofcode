
function hasUniqueChars(chars: String[]): boolean {
    return new Set(chars).size === chars.length
}

export function puzzle1(content: String){
    let indexStart: number = 0;
    const lastFour: Array<String> = [];

    [...content].every((value, index, array) => {
        if (index < 3) {
            lastFour.push(value)
            return true
        }
        lastFour.push(value)

        if (hasUniqueChars(lastFour)) {
            indexStart = index
            return false
        }

        lastFour.shift()
        return true
    })
    return (indexStart + 1)
}

export function puzzle2(content: String){
    let indexStart: number = 0;
    const lastFour: Array<String> = [];

    [...content].every((value, index, array) => {
        if (index < 13) {
            lastFour.push(value)
            return true
        }
        lastFour.push(value)

        if (hasUniqueChars(lastFour)) {
            indexStart = index
            return false
        }

        lastFour.shift()
        return true
    })

    return (indexStart + 1)
}
