import { loadPuzzle } from "../utils";
import { puzzle1, puzzle2 } from "./day7";

const PUZZLE_NUMBER = '7'
const PUZZLE_FOLDER = `2022_${PUZZLE_NUMBER}`

describe(`Day ${PUZZLE_NUMBER}`, () => {
    describe("Puzzle1", () => {
        it("example tree content", () => {
            const input = `$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k`
        expect(puzzle1(input)).toBe(95437);
        });


        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle1(content)).toBe(1334506);
        })
    })
    describe("Puzzle2", () => {

        it("example tree content", () => {
            const input = `$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k`
        expect(puzzle2(input)).toBe(24933642);
        });


        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle2(content)).toBe(7421137)
        })
    });
})