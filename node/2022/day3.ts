


function findTwoCompartments(rucksack: String): [String, String] {
    const sliceIndex = rucksack.length / 2
    return [
        rucksack.substring(0, sliceIndex),
        rucksack.substring(sliceIndex, rucksack.length)
    ]
}

function findSharedCharacters(string1: String, string2: String): String[] {
    return [...string1].filter(c => string2.includes(c))
}

function findPriorityCharacter(compartment1: String, compartment2: String): String {
    const sharedChars = findSharedCharacters(compartment1, compartment2)

    if (new Set(sharedChars).size === 1){
        return sharedChars[0]
    }

    throw new Error('something went wrong')
}


function getCharPriorityValue(char: String): number {
    let alphabetPosition = parseInt(char.toString(), 36) - 9

    if (char === char.toUpperCase()){
        alphabetPosition += 26
    }

    return alphabetPosition
}


function priorityOfRuckSack(ruckSack: String): number {
    const [firstCompartment, secondCompartment] = findTwoCompartments(ruckSack)
    const priorityChar = findPriorityCharacter(firstCompartment, secondCompartment)
    return getCharPriorityValue(priorityChar)
}



export function puzzle1(content: String){
    const lines = content.split('\n')

    return lines.reduce((previousCount, currentrucksack) => {
        return previousCount + priorityOfRuckSack(currentrucksack)
    }, 0)

}

function findGroupPriority(groups: String[]) : String {
    const [sack1, sack2, sack3] = groups
    
    const match1: String[] = findSharedCharacters(sack1, sack2)
    const match2: String[] = findSharedCharacters(sack2, sack3)

    const match3: String[] = findSharedCharacters(match1.join(''), match2.join(''))

    if(new Set(match3).size === 1) {
        return match3[0]
    }
    throw new Error('no matchfound')
}

export function puzzle2(content: String){
    const lines = content.split('\n')
    const groupsCount = lines.length / 3
    const groupedRuckSacks = [...new Array(groupsCount)].map(_ => lines.splice(0, 3))

    return groupedRuckSacks.reduce((previousCount, currentSack) => {
        const priorityChar = findGroupPriority(currentSack)
        return previousCount += getCharPriorityValue(priorityChar)
    }, 0)
}
