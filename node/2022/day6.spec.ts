import { loadPuzzle } from "../utils";
import { puzzle1, puzzle2 } from "./day6";

const PUZZLE_NUMBER = '6'
const PUZZLE_FOLDER = `2022_${PUZZLE_NUMBER}`

describe(`Day ${PUZZLE_NUMBER}`, () => {
    describe("Puzzle1", () => {
        it.each([
            {input: 'mjqjpqmgbljsphdztnvjfqwrcgsmlb', expected: 7},
            {input: 'bvwbjplbgvbhsrlpgdmjqwftvncz', expected: 5},
            {input: 'nppdvjthqldpwncqszvftbrmjlhg', expected: 6},
            {input: 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', expected: 10},
            {input: 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', expected: 11},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(puzzle1(input)).toBe(expected);
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle1(content)).toBe(1647);
        })
    })
    describe("Puzzle2", () => {
        it.each([
            {input: 'mjqjpqmgbljsphdztnvjfqwrcgsmlb', expected: 19},
            {input: 'bvwbjplbgvbhsrlpgdmjqwftvncz', expected: 23},
            {input: 'nppdvjthqldpwncqszvftbrmjlhg', expected: 23},
            {input: 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', expected: 29},
            {input: 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', expected: 26},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(puzzle2(input)).toBe(expected);
        })
        it("test puzzle using input", () => {
            const content: String = loadPuzzle(PUZZLE_FOLDER)
            expect(puzzle2(content)).toBe(2447)
        })
    });
})

