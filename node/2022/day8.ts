


function buildMap(content: string): Array<Array<number>> {
    return content.split('\n').map(line => [...line].map(c => parseInt(c)))
}

// function countTreeBorder(treeMap: Array<Array<number>>): number{
//     return (treeMap.length * 2) + (treeMap[0].length * 2) - 4
// }

function rowVisibleCoordinates(treeMap: Array<Array<number>>) : Array<String> {
    treeMap.reduce((prevCoordinates, currentRow, index, array) => {
        if(index === 0 || index === currentRow.length - 1){
            return prevCoordinates.concat(currentRow.map((_, columnIndex) => `${index},${columnIndex}`)
        }
    }, [])
}

function findVisibleTrees(treeMap: Array<Array<number>>): number {
    const rowCoordinats = rowVisibleCoordinates(treeMap)
    return 0
    // return treeMap.reduce((prevCount, currentRowValue, rowIndex, rowsArray) => {
    //     const rowSize = rowsArray.length - 1;
    //     if (rowIndex === 0) { return prevCount}
    //     if (rowIndex === rowSize) {return prevCount}

    //     return prevCount + currentRowValue.reduce((prevColumnCount, currentColumnValue, columnIndex, columnArray) => {
    //         const columnSize = columnArray.length - 1
    //         if (columnIndex === 0) {return prevCount}
    //         if (columnIndex === columnSize) {return prevCount}

    //         if (isVisibleTree(treeMap, columnIndex, rowIndex, rowSize, columnSize)){
    //             return prevColumnCount += 1
    //         }
    //         return prevColumnCount
    //     }, 0)
    // }, 0)
}


export function puzzle1(content: string){
    const treeMap = buildMap(content)
    // const countBorder = countTreeBorder(treeMap)
    const visibleTrees = findVisibleTrees(treeMap)
    console.log(visibleTrees)

    console.log(treeMap)
    // const lines = content.split('\n')
    // return lines.reduce((previousCount, currentrucksack) => {
    //     return previousCount
    // })
}

export function puzzle2(content: String){
    const lines = content.split('\n')
    return lines.reduce((previousCount, currentrucksack) => {
        return previousCount
    })
}
