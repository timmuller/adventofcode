import { loadPuzzle } from "../utils"
import { day1_puzzle1, day1_puzzle2_basement_position } from "./day1"

describe("Day 1", () => {
    describe("Question 1", () => {
        it.each([
            {input: '(())', expected: 0},
            {input: '()()', expected: 0},
            {input: '(((', expected: 3},
            {input: '(()(()(', expected: 3},
            {input: '))(((((', expected: 3},
            {input: '())', expected: -1},
            {input: '))(', expected: -1},
            {input: ')))', expected: -3},
            {input: ')())())', expected: -3},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day1_puzzle1(input)).toBe(expected);
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_1')
            expect(day1_puzzle1(content)).toBe(74)
        })
    })
    
    describe("Question 2", () => {
        it.each([
            {input: ')', expected: 1},
            {input: '()())', expected: 5}
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day1_puzzle2_basement_position(input)).toBe(expected);
        })
        
        fit("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_1')
            expect(day1_puzzle2_basement_position(content)).toBe(1795)
        })

    });
})