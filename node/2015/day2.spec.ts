import { loadPuzzle } from "../utils";
import { day2_puzzle1, day2_puzzle2 } from "./day2";

describe("Day 2", () => {
    describe("Question 1", () => {
        it.each([
            {input: '2x3x4', expected: 58},
            {input: '1x1x10', expected: 43},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day2_puzzle1(input)).toBe(expected);
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_2')
            expect(day2_puzzle1(content)).toBe(1586300)
        })
    })
    describe("Question 2", () => {
        it.each([
            {input: '2x3x4', expected: 34},
            {input: '1x1x10', expected: 14},
            {input: '10x1x1', expected: 14},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day2_puzzle2(input)).toBe(expected);
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_2')
            expect(day2_puzzle2(content)).toBe(3737498)
        })
    });
})

