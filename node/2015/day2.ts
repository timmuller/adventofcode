
export function day2_puzzle1(content: String) {
    return content.split('\n').reduce((previousValue, currentValue) => {
        const [length, width, height] = currentValue.split('x').map(x => (+x))

        const lengths = [length * width, width * height, length * height];
        const lowestValue = Math.min(...lengths)

        const total = (lengths[0] * 2) + (lengths[1] * 2) + (lengths[2] * 2) + lowestValue

        return previousValue + total;
    }, 0)
}

export function day2_puzzle2(content: String) {
    return content.split('\n').reduce((previousValue, currentValue) => {
        const lengths = currentValue.split('x').map(x => (+x));
        const sortByLength = lengths.sort((a, b) => (a < b ? -1 : 1));

        const ribbonWrap = sortByLength[0] * 2 + sortByLength[1] * 2;
        const ribbonBow = sortByLength[0] * sortByLength[1] * sortByLength[2];

        return previousValue + (ribbonBow + ribbonWrap);
    }, 0)
}