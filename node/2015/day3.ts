
interface Helpers {
    santa: Coordinate,
    robo: Coordinate
}

interface Coordinate {
    x: number,
    y: number,
}

function markCoordinate(map: Map<String, number>, coordinate: Coordinate){
    map.set(`${coordinate.x},${coordinate.y}`, 1)
    return map

}

export function day3_puzzle1(content: String) {

    const houses: Map<String, number> = new Map();
    const startCoordinate : Coordinate = {x: 0, y: 0}
    markCoordinate(houses, startCoordinate)

    Array.from(content).reduce((previousValue: Coordinate, currentValue) => {
        let newPosition: Coordinate;
        switch(currentValue){
            case '>':
                newPosition = {x: previousValue.x + 1, y: previousValue.y};
                break;
            case '<':
                newPosition = {x: previousValue.x - 1, y: previousValue.y};
                break;
            case '^':
                newPosition = {x: previousValue.x, y: previousValue.y + 1};
                break;
            case 'v':
                newPosition = {x: previousValue.x, y: previousValue.y - 1};
                break;
            default:
                throw new Error('Inrecognized character ' + currentValue)
        }

        markCoordinate(houses, newPosition)
        return newPosition
    }, startCoordinate)

    return houses.size
}

export function day3_puzzle2(content: String) {

    const houses: Map<String, number> = new Map();
    const startCoordinate : Coordinate = {x: 0, y: 0}
    markCoordinate(houses, startCoordinate)

    Array.from(content).reduce((previousValueHelper: Helpers, currentValue, currentIndex: number) => {
        let previousValue = !(currentIndex % 2) ? previousValueHelper.santa : previousValueHelper.robo;
        let newPosition: Coordinate;
        switch(currentValue){
            case '>':
                newPosition = {x: previousValue.x + 1, y: previousValue.y};
                break;
            case '<':
                newPosition = {x: previousValue.x - 1, y: previousValue.y};
                break;
            case '^':
                newPosition = {x: previousValue.x, y: previousValue.y + 1};
                break;
            case 'v':
                newPosition = {x: previousValue.x, y: previousValue.y - 1};
                break;
            default:
                throw new Error('Inrecognized character ' + currentValue)
        }

        markCoordinate(houses, newPosition)
        const santa = !(currentIndex % 2) ? newPosition : previousValueHelper.santa
        const robo = currentIndex % 2 ? newPosition : previousValueHelper.robo
        return {santa, robo}
    }, {santa: startCoordinate, robo: startCoordinate} as Helpers)

    return houses.size

}