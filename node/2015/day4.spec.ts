import { loadPuzzle } from "../utils";
import { day4_puzzle1, day4_puzzle2 } from "./day4";

describe("Day 4", () => {
    describe("Question 1", () => {
        it.each([
            {input: 'abcdef', expected: 609043},
            {input: 'pqrstuv', expected: 1048970},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day4_puzzle1(input)).toBe(expected);
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_4')
            expect(day4_puzzle1(content)).toBe(254575)
        })
    });
    describe("Question 2", () => {
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_4')
            expect(day4_puzzle2(content)).toBe(1038736)
        })
    });
})

