export function day1_puzzle1(content: String) {
    const totalCharacters = content.length;
    const upCharacters: Array<String> = Array.from(content).filter(c => c === '(');

    return upCharacters.length - (totalCharacters - upCharacters.length);
}

export function day1_puzzle2_basement_position(content: String) {
    const movements = Array.from(content).reduce((previousValue: number[], currentValue: string, currentIndex: number, array: string[]) => {
        const currentLevel = previousValue.length === 0 ? 0 : previousValue[currentIndex-1];
        const movement = currentValue === ')' ? 1 : -1;

        previousValue.push(currentLevel + movement);
        return previousValue
    }, [])
    
    return (movements.indexOf(1) + 1)
}