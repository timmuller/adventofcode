
const VOWELS = 'aeiou'
const NAUGHTY_COMBINATIONS = ['ab', 'cd', 'pq', 'xy']


function isNice(validateString: String): boolean {
    const hasNaughtyString = NAUGHTY_COMBINATIONS.filter(naughty => validateString.includes(naughty));
    if (hasNaughtyString.length){
        return false;
    }

    const vowelsMatches = Array.from(validateString).filter(char => VOWELS.includes(char))

    const hasDoubleCharInRow = Array.from(validateString).reduce((previousValue, currentValue, currentIndex, array) => {
        if (currentIndex == 0) { return  false };
        if (currentValue === array[currentIndex - 1]){
            return true
        }
        return previousValue
    }, false)

    return vowelsMatches.length >= 3 && hasDoubleCharInRow
}

export function day5_puzzle1(content: String) {
    // return count of nice matches
    const niceStrings = content.split('\n').filter(validateString => isNice(validateString));
    return niceStrings.length
}

function hasPairOfTwo(validateString: String): boolean {
    const pairs = Array.from(validateString).reduce((previousValue: {[key: string]: Array<number>}, currentValue, currentIndex, array) => {
        if (currentIndex == array.length -  1) {
            return previousValue
        }
        const currentPair = currentValue + array[currentIndex + 1]
        return {
            ...previousValue,
            [currentPair]: previousValue[currentPair] ? [...previousValue[currentPair], currentIndex] : [currentIndex]
        }
    }, {});
    const matches = Object.entries(pairs).filter(([key, value]) => (value.filter((v, index, array) => {
        return array.length !== index ? array[array.length - 1] - v >= 2 : false
    }).length >= 1));
    return matches.length >= 1
}

function hasRepeatingCharacter(validateString: String): boolean {
    const matchesArray = Array.from(validateString).map((element, index, array) => {
        return element === array[index - 2];
    }).filter(match => match);
    return matchesArray.length !== 0

}

function isNice2(validateString: String): boolean {
    const pairOfTwo: boolean = hasPairOfTwo(validateString)
    const repeatingChar: boolean = hasRepeatingCharacter(validateString)
    const isNice = pairOfTwo && repeatingChar
    return isNice
}

export function day5_puzzle2(content: String) {
    // return count of nice matches
    const niceStrings = content.split('\n').filter(validateString => isNice2(validateString));
    return niceStrings.length
}