import { loadPuzzle } from "../utils";
import { day6_puzzle1, day6_puzzle2 } from "./day6";

describe("Day 6", () => {
    describe("Question 1", () => {
        it.each([
            {input: 'turn on 0,0 through 999,999', expected: 1000000},
            {input: 'turn on 0,0 through 2,2', expected: 9},
            {input: 'turn on 0,0 through 0,0', expected: 1},
            {input: 'turn on 0,0 through 0,1', expected: 2},
            {input: 'turn on 0,0 through 9,9\nturn off 0,0 through 9,9', expected: 0},
            {input: 'turn on 0,0 through 9,9\ntoggle 0,0 through 9,9', expected: 0},
            {input: 'toggle 0,0 through 9,9', expected: 100},
            {input: 'turn off 4,4 through 4,4', expected: 0},
            {input: 'turn on 0,0 through 9,9\nturn off 4,4 through 4,4', expected: 99},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day6_puzzle1(input)).toBe(expected);
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_6')
            expect(day6_puzzle1(content)).toBe(569999);
        })
    })
    describe("Question 2", () => {
        it.each([
            {input: 'turn on 0,0 through 0,0', expected: 1},
            {input: 'toggle 0,0 through 999,999', expected: 2000000},
            {input: 'turn off 0,0 through 0,0', expected: 0},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day6_puzzle2(input)).toBe(expected);
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_6')
            expect(day6_puzzle2(content)).toBe(17836115)
        })
    
    });
})

