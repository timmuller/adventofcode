import { loadPuzzle } from "../utils";
import { day3_puzzle1, day3_puzzle2 } from "./day3";

describe("Day 3", () => {
    describe("Question 1", () => {
        it.each([
            {input: '>', expected: 2},
            {input: '^>v<', expected: 4},
            {input: '^v^v^v^v^v', expected: 2},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day3_puzzle1(input)).toBe(expected);
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_3')
            expect(day3_puzzle1(content)).toBe(2565)
        })
    })
    describe("Question 2", () => {
        it.each([
            {input: '^v', expected: 3},
            {input: '^>v<', expected: 3},
            {input: '^v^v^v^v^v', expected: 11},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day3_puzzle2(input)).toBe(expected);
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_3')
            expect(day3_puzzle2(content)).toBe(2639)
        })
    });
})

