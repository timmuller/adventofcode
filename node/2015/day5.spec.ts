import { loadPuzzle } from "../utils";
import { day5_puzzle1, day5_puzzle2 } from "./day5";

describe("Day 5", () => {
    describe("Question 1", () => {
        it.each([
            {input: 'ugknbfddgicrmopn', expected: 1},
            {input: 'aaa', expected: 1},
            {input: 'jchzalrnumimnmhp', expected: 0},
            {input: 'haegwjzuvuyypxyu', expected: 0},
            {input: 'dvszwmarrgswjxmb', expected: 0},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day5_puzzle1(input)).toBe(expected);
        })
        
        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_5')
            expect(day5_puzzle1(content)).toBe(255)
        })
    })
    describe.only("Question 2", () => {
        it.each([
            {input: 'qjhvhtzxzqqjkmpb', expected: 1},
            {input: 'qjZqj', expected: 0},
            {input: 'qkqj', expected: 0},
            {input: 'qjqj', expected: 1},
            {input: 'qjaqjbqjzkzqjsqj', expected: 1},
            {input: 'qjzkzqj', expected: 1},
            {input: 'qjaqaqj', expected: 1},
            {input: 'qjkqjkqj', expected: 0},
            {input: 'qjodobdbqj', expected: 1},

            {input: 'xxyxx', expected: 1},
            {input: 'uurcxstgmygtbstg', expected: 0},
            {input: 'ieodomkazucvgmuy', expected: 0},

            {input: 'aaa', expected: 0},
            {input: 'aaaa', expected: 1},
            {input: 'xyxy', expected: 1},
            {input: 'abcdddfgh', expected: 0},
            {input: 'aabcdcefgaa', expected: 1},
            {input: 'sknufchjdvccccta', expected: 1},
        ])("test $input to be $expected", ({input, expected}) => {
            expect(day5_puzzle2(input)).toBe(expected);
        })
        
        it("test puzzle using input help with reddit", () => {
            const content: String = loadPuzzle('2015_5', 'input_reddit.txt')
            expect(day5_puzzle2(content)).toBe(69)
        })

        it("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_5')
            expect(day5_puzzle2(content)).toBe(55)
        })
    });
})

