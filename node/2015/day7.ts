
class InsuffientWireData extends Error {}


export function day7_puzzle1(content: String): number {
    const instructions = content.split('\n');
    const wiringMap = executeInstructionsTillResolved(instructions);
    return wiringMap.get('a') || 0;
}

export function executeInstructionsTillResolved(instructions: string[], wiringMap?: Map<string, number>, iteration?: number): Map<string, number> {
    let loopingWiringMap = wiringMap ?? new Map<string, number>()
    let incompleteInstructions: string[] = []
    instructions.forEach(currentInstruction => {
        try {
            loopingWiringMap = runSignal(currentInstruction, loopingWiringMap)
        } catch (InsuffientWireData) {
            incompleteInstructions.push(currentInstruction)
        }
    });
    console.log(iteration, incompleteInstructions.length, loopingWiringMap.keys())
    if(iteration === 4) {
        throw new Error('stop')
    }
    if(incompleteInstructions.length) {
        return executeInstructionsTillResolved(incompleteInstructions, loopingWiringMap, iteration ? iteration + 1 : 1)
    }
    return loopingWiringMap
}


// hw LSHIFT 1 -> iq
// iq OR ip -> ir
// ir LSHIFT 1 -> jl
// jl OR jk -> jm
// jm LSHIFT 1 -> kg
// kg OR kf -> kh
// kh LSHIFT 1 -> lb
// lb OR la -> lc
// lc LSHIFT 1 -> lw

// lw OR lv
// lx -> a

function getSourceValue(wiring: Map<string, number>, source: string): number | undefined {
    if (Number(source)) {
        return Number(source)
    }
    return wiring.get(source);
}

function convertValue(inputValue: number): number {
    var n = 65536;
    return ((inputValue%n)+n)%n;
}

export function runSignal(inputString: string, wiring: Map<string, number>) {
    const [operation, target] = inputString.split(' -> ', 2)

    if (Number(operation)) {
        const fixedWireValue = Number(operation)
        wiring.set(target, fixedWireValue)
        return wiring
    }

    const operationSplit = operation.split(" ")
    if (operationSplit.length === 3) {
        const [source1, bitwiseOperation, source2] = operationSplit
        const source1Value = getSourceValue(wiring, source1);
        const source2Value = getSourceValue(wiring, source2);

        if((!source1Value || !source2Value)) { 
            throw new InsuffientWireData(inputString) 
        }

        if(bitwiseOperation == "AND") {
            wiring.set(target, convertValue(source1Value & source2Value))
        } else if (bitwiseOperation == "OR") {
            wiring.set(target, convertValue(source1Value | source2Value))
        } else if (bitwiseOperation == "LSHIFT") {
            wiring.set(target, convertValue(source1Value << source2Value))
        } else if (bitwiseOperation == "RSHIFT") {
            wiring.set(target, convertValue(source1Value >> source2Value))
        }
    }
    if (operationSplit.length === 2) {
        const [bitwiseOperation, source] = operationSplit
        const sourceValue = getSourceValue(wiring, source);
        
        if(!sourceValue) { 
            throw new InsuffientWireData(inputString) 
        }

        if (bitwiseOperation == "NOT"){
            const bitwiseValue = ~sourceValue
            const newValue = convertValue(bitwiseValue);
            wiring.set(target, newValue)

        }
    }
    return wiring 
}

export function day7_puzzle2(content: String): void {
    return 
}