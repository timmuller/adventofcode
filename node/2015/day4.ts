import * as crypto from 'crypto';


function getHashHex(content: String, currentNumber: Number) {
    const miningBytes = Buffer.from(content + currentNumber.toString(), 'utf8');
    const miningHash = crypto.createHash('md5').update(miningBytes).digest('hex');
    return miningHash
}

function mining(content: String, startingZeros: number = 5) {
    const assertionString = "0".repeat(startingZeros);
    const lengthContent = content.length;
    const startNumber = 0;
    const maxNumber = Math.pow(10, lengthContent);
    const totalIterations = maxNumber - startNumber;


    const match = Array.from(Array(totalIterations).keys()).find(
        currentIteration => {
            const currentNumber = currentIteration + startNumber;
            const miningHash = getHashHex(content, currentNumber);
            return miningHash.substring(0, startingZeros) === assertionString; 
        }
    )
    return match ? match + startNumber : 0;
}

export function day4_puzzle1(content: String) {
    return mining(content, 5);
}

export function day4_puzzle2(content: String) {
    return mining(content, 6);
}