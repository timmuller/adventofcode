import { loadPuzzle } from "../utils";
import { day7_puzzle1, day7_puzzle2, executeInstructionsTillResolved, runSignal } from "./day7";

describe("Day 7", () => {
    describe.only("Question 1", () => {
        it('test order of execution', () => {
            expect(day7_puzzle1("123 -> a")).toEqual(123)
        })

        it('test wiring map', () => {
            const steps = [
                "h OR i -> z",
                "NOT a -> ti",
                "5 -> a",
                "123 -> x",
                "456 -> y",
                "x AND y -> d",
                "x OR y -> e",
                "x LSHIFT 2 -> f",
                "y RSHIFT 2 -> g",
                "NOT x -> h",
                "NOT y -> i",
            ]

            const testWiring = executeInstructionsTillResolved(steps);

            expect(testWiring.get('x')).toEqual(123)
            expect(testWiring.get('y')).toEqual(456)
            expect(testWiring.get('d')).toEqual(72)
            expect(testWiring.get('e')).toEqual(507)
            expect(testWiring.get('f')).toEqual(492)
            expect(testWiring.get('g')).toEqual(114)
            expect(testWiring.get('h')).toEqual(65412)
            expect(testWiring.get('i')).toEqual(65079)
            expect(testWiring.get('ti')).toEqual(65530)
            expect(testWiring.get('z')).toEqual(65463)
        })

        it.only("test puzzle using input", () => {
            const content: String = loadPuzzle('2015_7')
            expect(day7_puzzle1(content)).toBe(569999);
        })
    })
    describe("Question 2", () => {
        // it.each([
        //     // {input: 'turn on 0,0 through 0,0', expected: 1},
        //     // {input: 'toggle 0,0 through 999,999', expected: 2000000},
        //     // {input: 'turn off 0,0 through 0,0', expected: 0},
        // ])("test $input to be $expected", ({input, expected}) => {
        //     expect(day7_puzzle2(input)).toBe(expected);
        // })
        
        // it("test puzzle using input", () => {
        //     const content: String = loadPuzzle('2015_7')
        //     expect(day7_puzzle2(content)).toBe(17836115)
        // })
    
    });
})

