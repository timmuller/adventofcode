
enum Action {
    TURN_ON = 'TURN_ON',
    TURN_OFF = "TURN_OFF",
    TOGGLE = "TOGGLE",
}

interface Coordinate {
    x: number;
    y: number;
}

function getActionByText(actionText: String): Action {
    if(actionText === 'turn on') {
        return Action.TURN_ON
    }
    if (actionText === 'turn off'){
        return Action.TURN_OFF
    }
    return Action.TOGGLE
}

function parseInstructions(instruction: String) {
    const instructionMatch = instruction.match(/^([a-z ]+) (\d+,\d+) through (\d+,\d+)$/);
    if (!instructionMatch){
        throw new Error(`Cannot parse instruction ${instruction}`)
    }
    const [actionText, fromCoordinate, toCoordinate] = [instructionMatch[1], instructionMatch[2], instructionMatch[3]]
    return [getActionByText(actionText), fromCoordinate, toCoordinate];
}

function generateGrid(fromCoordinate: String, toCoordinate: String) {
    const [fromX, fromY] = fromCoordinate.split(',').map(Number)
    const [toX, toY] = toCoordinate.split(',').map(Number)

    const rangeX = Array.from(Array((toX + 1) - fromX).keys()).map(i => i + fromX);
    const rangeY = Array.from(Array((toY + 1) - fromY).keys()).map(i => i + fromY);

    const coordinates = rangeX.flatMap(currentX => rangeY.map(currentY => `${currentX},${currentY}`))
    return coordinates;
}

export function day6_puzzle1(content: String): Number {
    const instructions = content.split('\n')

    const lightsOn = instructions.reduce((prevField, currentInstruction) => {
        const [action, fromCoordinate, toCoordinate] = parseInstructions(currentInstruction)
        const involvedCoordinates = generateGrid(fromCoordinate, toCoordinate)

        if(action === Action.TURN_ON){
            involvedCoordinates.forEach(c => prevField.add(c))
        }
        if(action === Action.TURN_OFF){
            involvedCoordinates.forEach(c => prevField.delete(c))
        }
        if(action === Action.TOGGLE){
            involvedCoordinates.forEach(c => prevField.has(c) ? prevField.delete(c) : prevField.add(c))
        }

        return prevField
    }, new Set())

    return lightsOn.size
}

export function day6_puzzle2(content: String) {
    const instructions = content.split('\n')
    const brightnessMap = instructions.reduce((prevField, currentInstruction) => {
        const [action, fromCoordinate, toCoordinate] = parseInstructions(currentInstruction)
        const involvedCoordinates = generateGrid(fromCoordinate, toCoordinate)
        
        if(action === Action.TURN_ON){
            involvedCoordinates.forEach(coordinate => prevField.set(coordinate, (prevField.get(coordinate) || 0) + 1));
        }
        if(action === Action.TURN_OFF){
            involvedCoordinates.forEach(
                coordinate => {
                    let reducedValue = (prevField.get(coordinate) || 0) - 1;
                    if (reducedValue < 0) {
                        reducedValue = 0;
                    }
                    prevField.set(coordinate, reducedValue)
                });
        }
        if(action === Action.TOGGLE){
            involvedCoordinates.forEach(coordinate => prevField.set(coordinate, (prevField.get(coordinate) || 0) + 2));
        }
        return prevField
    }, new Map<string, number>())

    return Array.from(brightnessMap.values()).reduce((prevNum, currentNum) => (prevNum + currentNum), 0)
}