import { readFileSync } from 'fs';

export function loadPuzzle(name: String, filename: String = 'input.txt'): String {
    const path = `../puzzles/${name}/${filename}`;
    const fileContent = readFileSync(path, 'utf8')
    return fileContent
}
