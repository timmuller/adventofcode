


def get_input_data(filename):
    with open(filename, 'r') as fh:
        return fh.read()
